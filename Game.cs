﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     A game of Monopoly.
    /// </summary>
    [Serializable]
    public class Game : ISerializable
    {
        public delegate void AdvancePlayerHandler(Game game, AdvancePlayerEventArgs args);

        public delegate void EndRollHandler(Game game, EndRollEventArgs args);

        public delegate void EndTurnHandler(Game game, EndTurnEventArgs args);

        public delegate void HousePurchasedHandler(Game game, HousePurchasedEventArgs args);

        public delegate void HouseSoldHandler(Game game, HouseSoldEventArgs args);

        public delegate void JailRollHandler(Game game, JailRollEventArgs args);

        public delegate void MovePlayerHandler(Game game, MovePlayerEventArgs args);

        public delegate void NewTurnHandler(Game game, NewTurnEventArgs args);

        public delegate void PlayerCreatedHandler(Game game, PlayerCreatedEventArgs args);

        public delegate void PropertyMortgagedHandler(Game game, PropertyMortgagedEventArgs args);

        public delegate void PropertyUnmortgagedHandler(Game game, PropertyUnmortgagedEventArgs args);

        public delegate void RolledHandler(Game game, RolledEventArgs args);

        private readonly IEnumerator<Card> _chanceCards;
        private readonly List<Card> _chanceDeck;
        private readonly IEnumerator<Card> _communityChestCards;
        private readonly List<Card> _communityChestDeck;

        private readonly Stack<Player> _currentInput = new Stack<Player>();

        private readonly Dictionary<string, IData> _data = new Dictionary<string, IData>();

        private readonly Dictionary<string, BoardSquare> _locations = new Dictionary<string, BoardSquare>();

        private readonly Dictionary<string, IMod> _mods;

        private readonly List<Player> _players;

        private readonly Random _rand = new Random();

        private readonly Dictionary<Property, PropertyState> _states =
            new Dictionary<Property, PropertyState>();

        private IEnumerator<Player> _playerSequence;

        /// <summary>
        ///     Constructs a new game.
        /// </summary>
        /// <param name="shell">The shell to use for input and output.</param>
        /// <param name="board">The board to use for play.</param>
        /// <param name="players">All player names.</param>
        /// <param name="chanceCards">All Chance cards.</param>
        /// <param name="communityChestCards">All Community Chest cards.</param>
        /// <param name="mods">A list of all mod IDs.</param>
        public Game(IShell shell, Board board, IEnumerable<string> players, IEnumerable<Card> chanceCards,
            IEnumerable<Card> communityChestCards, IDictionary<string, IMod> mods)
        {
            _mods = new Dictionary<string, IMod>(mods);
            Board = board;
            Shell = shell;
            shell.Game = this;
            Properties = board.Squares.WhereCast<Property>().ToList().AsReadOnly();
            _chanceDeck = chanceCards.Shuffle().ToList();
            _chanceCards = _chanceDeck.Cycle().GetEnumerator();
            _chanceCards.MoveNext();
            _communityChestDeck = communityChestCards.Shuffle().ToList();
            _communityChestCards = _communityChestDeck.Cycle().GetEnumerator();
            _communityChestCards.MoveNext();
            _players = players.Select(s => new Player(this, s)).ToList();
            UpdateSequence();
            foreach (var player in _players)
            {
                _locations[player.Name] = board[0];
            }
        }

        public object TurnToken { get; private set; }

        /// <summary>
        ///     A collection of all properties that are in play. Not all are owned.
        /// </summary>
        public IReadOnlyCollection<Property> Properties { get; }

        /// <summary>
        ///     The last rolled sum of the dice.
        /// </summary>
        public int LastDieRoll { get; set; }

        /// <summary>
        ///     The <see cref="Player" /> whose turn it currently is.
        /// </summary>
        public Player CurrentTurn { get; private set; }

        public IReadOnlyDictionary<string, IMod> Mods => new ReadOnlyDictionary<string, IMod>(_mods);

        /// <summary>
        ///     The <see cref="Player" /> who currently provides the input. For use
        ///     when you need to ask something to a player whose turn it isn't.
        /// </summary>
        /// <remarks>
        ///     This is actually the top element of a stack. For the game to behave
        ///     normally, <c>null</c> should be assigned once for each time a value
        ///     is assigned.
        /// </remarks>
        public Player CurrentInput
        {
            get => _currentInput.Count == 0 ? CurrentTurn : _currentInput.Peek();
            set
            {
                if (value == null)
                {
                    if (_currentInput.Count != 0)
                    {
                        var prev = _currentInput.Pop();
                        if (prev != CurrentInput)
                        {
                            prev.Notify(string.Format(Strings.SwitchBackInput, CurrentInput));
                            CurrentInput.FlushMessages();
                        }
                    }
                }
                else
                {
                    _currentInput.Push(value);
                    if (value != CurrentInput)
                    {
                        CurrentInput.Notify(string.Format(Strings.SwitchInput, value));
                        CurrentInput.FlushMessages();
                    }
                }
            }
        }

        /// <summary>
        ///     Used to set <see cref="CurrentInput" />, but without sending
        ///     any messages.
        /// </summary>
        public Player CurrentInputNoMessage
        {
            set
            {
                if (value == null)
                {
                    if (_currentInput.Count != 0)
                    {
                        _currentInput.Pop();
                    }
                }
                else
                {
                    _currentInput.Push(value);
                }
            }
        }

        /// <summary>
        ///     The <see cref="IShell" /> that this <see cref="Game" /> is using.
        /// </summary>
        public IShell Shell { get; set; }

        /// <summary>
        ///     A list of all <see cref="Player" />s in this <see cref="Game" />.
        /// </summary>
        public IReadOnlyList<Player> Players => _players;

        /// <summary>
        ///     This game's current board.
        /// </summary>
        public Board Board { get; }

        /// <summary>
        ///     Gets the current state of a <see cref="Property" /> in this
        ///     game.
        /// </summary>
        /// <param name="property">The property to look up.</param>
        /// <returns>
        ///     The state of <paramref name="property" />, or null if unowned.
        /// </returns>
        public PropertyState this[Property property]
        {
            get => _states.TryGetValue(property, out var state) ? state : null;
            set => _states[property] = value;
        }

        public ColorPropertyState this[ColorProperty property]
        {
            get => _states.TryGetValue(property, out var state) ? (ColorPropertyState)state : null;
            set => _states[property] = value;
        }

        public IData this[IMod mod]
        {
            get => this[mod.Id];
            set
            {
                if (value.GetType().IsSerializable)
                {
                    _data[mod.Id] = value;
                }
                else
                {
                    throw new ArgumentException($"{nameof(value)} is not serializable");
                }
            }
        }

        public IData this[string modId]
        {
            get => _data.TryGetValue(modId, out var mod) ? mod : null;
            set
            {
                if (value.GetType().IsSerializable)
                {
                    if (_data.TryGetValue(modId, out var _))
                    {
                        _data[modId] = value;
                    }
                }
                else
                {
                    throw new ArgumentException($"{nameof(value)} is not serializable");
                }
            }
        }

        public BoardSquare this[Player player] => _locations[player.Name];

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            var chanceDeck = _chanceDeck.Rotate(_chanceCards.Current);
            info.AddValue("chancedeck", chanceDeck);
            var communityChestDeck = _communityChestDeck.Rotate(_communityChestCards.Current);
            info.AddValue("communitychestdeck", communityChestDeck);
            info.AddValue("mods", _mods.Keys);
            var data = new Dictionary<string, IData>();
            foreach (var pair in _data)
            {
                data[pair.Key] = pair.Value;
            }
            info.AddValue("data", data);
            var locations = new Dictionary<string, int>();
            info.AddValue("players", _players);
            foreach (var pair in _locations)
            {
                locations[pair.Key] = Board.IndexOf(pair.Value);
            }
            info.AddValue("locations", locations);
            info.AddValue("currentturn", CurrentTurn.Name);
            var states = new Dictionary<string, PropertyState>();
            foreach (var pair in _states)
            {
                states[pair.Key.Name] = pair.Value;
            }
            info.AddValue("states", states);
            info.AddValue("board", Board);
        }

        public event PlayerCreatedHandler PlayerCreated;

        public event Player.PropertyPurchasedHandler PropertyPurchased
        {
            add
            {
                foreach (var player in _players)
                {
                    player.PropertyPurchased += value;
                }
            }
            remove
            {
                foreach (var player in _players)
                {
                    player.PropertyPurchased -= value;
                }
            }
        }

        public event Player.JailedHandler Jailed
        {
            add
            {
                foreach (var player in _players)
                {
                    player.Jailed += value;
                }
            }
            remove
            {
                foreach (var player in _players)
                {
                    player.Jailed -= value;
                }
            }
        }

        public event Player.BankruptedHandler Bankrupted
        {
            add
            {
                foreach (var player in _players)
                {
                    player.Bankrupted += value;
                }
            }
            remove
            {
                foreach (var player in _players)
                {
                    player.Bankrupted -= value;
                }
            }
        }

        internal void UpdatePlayers()
        {
            foreach (var player in _players)
            {
                PlayerCreated?.Invoke(this, new PlayerCreatedEventArgs(player));
            }
        }

        /// <summary>
        ///     Draws the top Chance card.
        /// </summary>
        /// <returns>The top card.</returns>
        public Card DrawChance()
        {
            var card = _chanceCards.Current;
            _chanceCards.MoveNext();
            return card;
        }

        /// <summary>
        ///     Draws the top Community Chest card.
        /// </summary>
        /// <returns>The top card.</returns>
        public Card DrawCommunityChest()
        {
            var card = _communityChestCards.Current;
            _communityChestCards.MoveNext();
            return card;
        }

        /// <summary>
        ///     Peeks at the top Chance card.
        /// </summary>
        /// <returns>The top card.</returns>
        public Card PeekChance() => _chanceCards.Current;

        /// <summary>
        ///     Peeks at the top Community Chest card.
        /// </summary>
        /// <returns>The top card.</returns>
        public Card PeekCommunityChest() => _communityChestCards.Current;

        /// <summary>
        ///     Updates <see cref="_playerSequence" />. Call this after updating
        ///     <see cref="_players" />.
        /// </summary>
        /// <param name="player">
        ///     If non-null, <see cref="CurrentTurn" /> will
        ///     be set to this <see cref="Player" />.
        /// </param>
        private void UpdateSequence(Player player = null)
        {
            _playerSequence = _players.Cycle().GetEnumerator();
            _playerSequence.MoveNext();
            if (player != null)
            {
                while (_playerSequence.Current != player)
                {
                    _playerSequence.MoveNext();
                }
            }
            CurrentTurn = _playerSequence.Current;
        }

        public event RolledHandler Rolled;

        /// <summary>
        ///     Rolls the dice.
        /// </summary>
        /// <returns>The two die rolls.</returns>
        public (int, int) Roll(bool turn = true, bool sendMessage = true)
        {
            int rand1 = _rand.Next(1, 7);
            int rand2 = _rand.Next(1, 7);
            var args = new RolledEventArgs(rand1, rand2, turn, sendMessage);
            Rolled?.Invoke(this, args);
            if (args.SendMessage)
            {
                CurrentInput.Notify(string.Format(Strings.RollResult, args.Die1, args.Die2));
            }
            return (args.Die1, args.Die2);
        }

        public event NewTurnHandler NewTurn;

        public event HousePurchasedHandler HousePurchased;

        public event HouseSoldHandler HouseSold;

        public event PropertyMortgagedHandler PropertyMortgaged;

        public event PropertyUnmortgagedHandler PropertyUnmortgaged;

        public event JailRollHandler JailRoll;

        /// <summary>
        ///     Runs the game. Blocks until the game is complete or stopped.
        /// </summary>
        /// <returns>
        ///     Whether or not the game was completed.
        /// </returns>
        public bool Run()
        {
            try
            {
                while (true)
                {
                    if (Players.Count == 1)
                    {
                        break;
                    }
                    _playerSequence.MoveNext();
                    var prevCurrent = CurrentTurn;
                    CurrentTurn = _playerSequence.Current;
                    // ReSharper disable once PossibleNullReferenceException
                    CurrentTurn.Notify(string.Format(Strings.TurnChanged, CurrentTurn));
                    if (CurrentTurn.IsBankrupted)
                    {
                        CurrentTurn.Notify(Strings.BankruptcyRemove);
                        _players.Remove(CurrentTurn);
                        UpdateSequence(prevCurrent);
                        continue;
                    }
                    TurnToken = new object();
                    NewTurn?.Invoke(this, NewTurnEventArgs.Empty);
                    _currentInput.Clear();
                    CurrentTurn.FlushMessages();
                    string cmd;
                    while ((cmd =
                               Shell.Expect(
                                   Strings.TurnChoice,
                                   Strings.TurnChoiceTake, Strings.TurnChoiceBuy, Strings.TurnChoiceSell,
                                   Strings.TurnChoiceMortgage, Strings.TurnChoiceUnmortgage)) != Strings.TurnChoiceTake)
                    {
                        Shell.CanSave = false;
                        // ReSharper disable once SwitchStatementMissingSomeCases
                        if (cmd == Strings.TurnChoiceBuy)
                        {
                            var cproperties = Properties.WhereCast<ColorProperty>().ToArray();
                            var cproperty = Shell.Expect(Strings.EnterPropertyName, cproperties, p => p.Name);
                            var cstate = this[cproperty];
                            if (cstate == null)
                            {
                                CurrentTurn.Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (cstate.Owner != CurrentTurn)
                            {
                                CurrentTurn.Notify(string.Format(Strings.NoOwnPropertyOther, cstate.Owner));
                                continue;
                            }
                            if (cstate.Houses == cstate.Property.MaxHouses)
                            {
                                CurrentTurn.Notify(Strings.MaxHousesBuilt);
                                continue;
                            }
                            var grouped = cproperties.Where(p => p.ColorGroup == cproperty.ColorGroup).ToArray();
                            if (!CurrentTurn.Properties.ContainsAll(grouped))
                            {
                                CurrentTurn.Notify(string.Format(Strings.NotAllPropertiesOwned, cproperty.ColorGroup));
                                continue;
                            }
                            if (grouped.Any(p => this[p].IsMortgaged))
                            {
                                CurrentTurn.Notify(Strings.NotAllPropertiesUnmortgaged);
                                continue;
                            }
                            var others = grouped.Except(cproperty).Select(p => this[p]);
                            var notEnough = others.FirstOrDefault(p => p.Houses < cstate.Houses);
                            if (notEnough != null)
                            {
                                CurrentTurn.Notify(string.Format(Strings.NotEnoughHouses, notEnough));
                                continue;
                            }
                            if (CurrentTurn.GetTotalPayable() < cproperty.ColorGroup.HousePrice)
                            {
                                CurrentTurn.Notify(Strings.CannotAffordHouse);
                                continue;
                            }
                            var args = new HousePurchasedEventArgs(CurrentTurn, cstate, false);
                            HousePurchased?.Invoke(this, args);
                            _currentInput.Clear();
                            if (!args.Cancelled)
                            {
                                cstate.Houses++;
                                CurrentTurn.Pay(cproperty.ColorGroup.HousePrice);
                            }
                        }
                        else if (cmd == Strings.TurnChoiceSell)
                        {
                            var cproperties = Properties.WhereCast<ColorProperty>().ToArray();
                            var cproperty = Shell.Expect(Strings.EnterPropertyName, cproperties, p => p.Name);
                            var cstate = this[cproperty];
                            if (cstate == null)
                            {
                                CurrentTurn.Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (cstate.Owner != CurrentTurn)
                            {
                                CurrentTurn.Notify(string.Format(Strings.NoOwnPropertyOther, cstate.Owner));
                                continue;
                            }
                            if (cstate.Houses == 0)
                            {
                                CurrentTurn.Notify(Strings.NoHousesOnProperty);
                                continue;
                            }
                            var others = cproperties
                                .Where(p => p.ColorGroup == cproperty.ColorGroup)
                                .Except(cproperty).Select(p => this[p]);
                            var tooMany = others.FirstOrDefault(p => p.Houses > cstate.Houses);
                            if (tooMany != null)
                            {
                                CurrentTurn.Notify(string.Format(Strings.TooManyHouses, tooMany));
                                continue;
                            }
                            var args = new HouseSoldEventArgs(CurrentTurn, cstate, false);
                            HouseSold?.Invoke(this, args);
                            _currentInput.Clear();
                            if (!args.Cancelled)
                            {
                                cstate.Houses--;
                                CurrentTurn.Money += cproperty.ColorGroup.HousePrice / 2;
                            }
                        }
                        else if (cmd == Strings.TurnChoiceMortgage)
                        {
                            var property = Shell.Expect(Strings.EnterPropertyName, Properties, p => p.Name);
                            var state = this[property];
                            if (state == null)
                            {
                                CurrentTurn.Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (state.Owner != CurrentTurn)
                            {
                                CurrentTurn.Notify(string.Format(Strings.NoOwnPropertyOther, state.Owner));
                                continue;
                            }
                            if (state.IsMortgaged)
                            {
                                CurrentTurn.Notify(Strings.AlreadyMortgaged);
                                continue;
                            }
                            if (property is ColorProperty)
                            {
                                var others = Properties.Except(property).WhereCast<ColorProperty>()
                                    .Select(p => this[p]);
                                var hasHouses = others.FirstOrDefault(p => p.Houses != 0);
                                if (hasHouses != null)
                                {
                                    CurrentTurn.Notify(string.Format(Strings.PropertyHasHouses, hasHouses));
                                    continue;
                                }
                            }
                            var args = new PropertyMortgagedEventArgs(CurrentTurn, state, false);
                            PropertyMortgaged?.Invoke(this, args);
                            _currentInput.Clear();
                            if (!args.Cancelled)
                            {
                                state.IsMortgaged = true;
                                CurrentTurn.Money += property.MortgageValue;
                            }
                        }
                        else if (cmd == Strings.TurnChoiceUnmortgage)
                        {
                            var property = Shell.Expect(Strings.EnterPropertyName, Properties, p => p.Name);
                            var state = this[property];
                            if (state == null)
                            {
                                CurrentTurn.Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (state.Owner != CurrentTurn)
                            {
                                CurrentTurn.Notify(string.Format(Strings.NoOwnPropertyOther, state.Owner));
                                continue;
                            }
                            if (!state.IsMortgaged)
                            {
                                CurrentTurn.Notify(Strings.NotMortgaged);
                                continue;
                            }
                            if (CurrentTurn.GetTotalPayable() < property.UnmortgageValue)
                            {
                                CurrentTurn.Notify(Strings.CannotAffordUnmortgage);
                                continue;
                            }
                            var args = new PropertyUnmortgagedEventArgs(CurrentTurn, state, false);
                            PropertyUnmortgaged?.Invoke(this, args);
                            _currentInput.Clear();
                            if (!args.Cancelled)
                            {
                                state.IsMortgaged = false;
                                CurrentTurn.Pay(property.UnmortgageValue);
                            }
                        }
                    }
                    if (CurrentTurn.InJail)
                    {
                        while (true)
                        {
                            cmd = Shell.Expect(Strings.JailChoice, Strings.JailChoiceFree, Strings.JailChoicePay,
                                Strings.JailChoiceRoll);
                            int roll1, roll2;
                            if (cmd == Strings.JailChoiceFree)
                            {
                                if (CurrentTurn.GetOutOfJailFreeCards == 0)
                                {
                                    CurrentTurn.Notify(Strings.NoJailCards);
                                    continue;
                                }
                                CurrentTurn.GetOutOfJailFreeCards--;
                                CurrentTurn.InJail = false;
                                break;
                            }
                            else if (cmd == Strings.JailChoicePay)
                            {
                                if (CurrentTurn.GetTotalPayable() < 50)
                                {
                                    CurrentTurn.Notify(Strings.CannotAffordBail);
                                    continue;
                                }
                                CurrentTurn.Pay(50);
                                CurrentTurn.Notify(Strings.PaidBail);
                                CurrentTurn.InJail = false;
                                break;
                            }
                            else if (cmd == Strings.JailChoiceRoll)
                            {
                                (roll1, roll2) = Roll(false);
                                if (roll1 == roll2)
                                {
                                    var args = new JailRollEventArgs(CurrentTurn, false);
                                    JailRoll?.Invoke(this, args);
                                    _currentInput.Clear();
                                    if (!args.Cancelled)
                                    {
                                        CurrentTurn.Notify(Strings.JailRolledDoubles);
                                        goto roll;
                                    }
                                }
                                CurrentTurn.JailTurns++;
                                if (CurrentTurn.JailTurns == 3)
                                {
                                    CurrentTurn.Notify(
                                        Strings.MustPayBail);
                                    CurrentTurn.Pay(50);
                                    CurrentTurn.InJail = false;
                                    goto roll;
                                }
                                else
                                {
                                    goto turn;
                                }
                            }
                            (roll1, roll2) = Roll();
                            roll:
                            LastDieRoll = roll1 + roll2;
                            Advance(CurrentTurn, roll1 + roll2);
                            _currentInput.Clear();
                            break;
                        }
                        continue;
                    }
                    // stupid local variable collision

                    int die1 = 0, die2 = 0;
                    int doubles = 0;
                    while (die1 == die2)
                    {
                        (die1, die2) = Roll();
                        if (die1 == die2)
                        {
                            doubles++;
                            if (doubles == 3)
                            {
                                CurrentTurn.InJail = true;
                                break;
                            }
                        }
                        LastDieRoll = die1 + die2;
                        Advance(CurrentTurn, die1 + die2);
                        EndRoll?.Invoke(this, new EndRollEventArgs(CurrentTurn));
                        _currentInput.Clear();
                    }
                    turn:
                    EndTurn?.Invoke(this, new EndTurnEventArgs(CurrentTurn));
                    _currentInput.Clear();
                }
            }
            catch (GameSaveInterrupt)
            {
                return false;
            }
            return true;
        }

        public event AdvancePlayerHandler AdvancePlayer;

        public event EndRollHandler EndRoll;

        public event EndTurnHandler EndTurn;

        /// <summary>
        ///     Moves the player forward some number of squares.
        /// </summary>
        /// <param name="player">The player to move.</param>
        /// <param name="squares">The number of squares to move.</param>
        /// <param name="passGo">
        ///     Whether or not to collect $200 upon passing Go.
        /// </param>
        /// <param name="handleLand">
        ///     Whether or not to activate the square that is landed on.
        /// </param>
        public void Advance(Player player, int squares, bool passGo = true, bool handleLand = true)
        {
            squares %= Board.Size;
            if (squares < 0)
            {
                squares = Board.Size + squares;
            }
            var loc = _locations[player.Name];
            var end = Board[Board.IndexOf(loc) + squares];
            var args = new AdvancePlayerEventArgs(player, loc, end, passGo, handleLand, false);
            AdvancePlayer?.Invoke(this, args);
            if (args.Cancelled)
            {
                return;
            }
            if (args.PassGo)
            {
                var board = Board.StartingFrom(_locations[player.Name]);
                foreach (var bSquare in board)
                {
                    if (bSquare is Go)
                    {
                        player.Notify(Strings.PassGo);
                        player.Money += 200;
                    }
                    if (bSquare == args.EndingUp)
                    {
                        break;
                    }
                }
            }
            _locations[player.Name] = args.EndingUp;
            player.Notify(string.Format(Strings.LandedOnSpace, args.EndingUp));
            if (args.HandleLand)
            {
                args.EndingUp.HandleLand(player);
            }
        }

        public event MovePlayerHandler MovePlayer;

        /// <summary>
        ///     Moves the player to a particular square.
        /// </summary>
        /// <param name="player">The player to move.</param>
        /// <param name="square">The square to move to.</param>
        /// <param name="passGo">
        ///     Whether or not to collect $200 upon passing Go.
        /// </param>
        /// <param name="handleLand">
        ///     Whether or not to activate the square that is landed on.
        /// </param>
        public void SetSquare(Player player, BoardSquare square, bool passGo = true, bool handleLand = true)
        {
            var start = _locations[player.Name];
            var args = new MovePlayerEventArgs(player, start, square, passGo, handleLand, false);
            MovePlayer?.Invoke(this, args);
            if (args.Cancelled)
            {
                return;
            }
            if (args.PassGo)
            {
                foreach (var bSquare in Board.StartingFrom(_locations[player.Name]))
                {
                    if (bSquare is Go)
                    {
                        player.Notify(Strings.PassGo);
                        player.Money += 200;
                    }
                    if (args.EndingUp == bSquare)
                    {
                        break;
                    }
                }
            }
            _locations[player.Name] = args.EndingUp;
            player.Notify(string.Format(Strings.LandedOnSpace, args.EndingUp));
            if (args.HandleLand)
            {
                args.EndingUp.HandleLand(player);
            }
        }

        /// <summary>
        ///     Moves the player to the first square of a particular type.
        /// </summary>
        /// <remarks>
        ///     Useful for doing things like moving to Go.
        /// </remarks>
        /// <param name="player">The player to move.</param>
        /// <param name="squareType">
        ///     The <see cref="Type" /> of the target square.
        /// </param>
        /// <param name="passGo">
        ///     Whether or not to collect $200 upon passing Go.
        /// </param>
        /// <param name="handleLand">
        ///     Whether or not to activate the square that is landed on.
        /// </param>
        public void SetSquare(Player player, Type squareType, bool passGo = true, bool handleLand = true)
        {
            SetSquare(player,
                Board.Squares.First(s => s.GetType() == squareType || s.GetType().IsSubclassOf(squareType)), passGo,
                handleLand);
        }

        /// <summary>
        ///     Moves the player to the first square of a particular type.
        /// </summary>
        /// <remarks>
        ///     Useful for doing things like moving to Go. Do not use for moving to
        ///     Jail; instead, use <see cref="Player.InJail" />.
        /// </remarks>
        /// <param name="player">The player to move.</param>
        /// <param name="passGo">
        ///     Whether or not to collect $200 upon passing Go.
        /// </param>
        /// <param name="handleLand">
        ///     Whether or not to activate the square that is landed on.
        /// </param>
        /// <typeparam name="T">The type of the target square.</typeparam>
        public void SetSquare<T>(Player player, bool passGo = true, bool handleLand = true)
        {
            SetSquare(player, Board.Squares.First(s => s is T), passGo, handleLand);
        }

        public void AuctionProperty(Property property)
        {
            CurrentInput.Notify(string.Format(Strings.StartAuction, property));
            var players = Players.ToList();
            Player lastBidder = null;
            uint bid = 0;
            while (true)
            {
                var remove = new List<Player>();
                foreach (var player in players)
                {
                    CurrentInput = player;
                    if (player.GetTotalPayable() >= bid)
                    {
                        player.Notify(Strings.BidTooHigh);
                        remove.Add(player);
                    }
                    else if (Shell.Confirm(Strings.BidQuery))
                    {
                        uint amount = Shell.Expect(Strings.EnterBid, (string s, out uint result) =>
                        {
                            if (uint.TryParse(s, out result))
                            {
                                if (result > player.GetTotalPayable())
                                {
                                    Shell.Write(Strings.CannotAffordBid);
                                    return false;
                                }
                                // ReSharper disable once AccessToModifiedClosure
                                if (result <= bid)
                                {
                                    Shell.Write(Strings.InvalidBid);
                                    return false;
                                }
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        });
                        bid = amount;
                        lastBidder = player;
                    }
                    else
                    {
                        remove.Add(player);
                    }
                    CurrentInputNoMessage = null;
                }
                players = players.Except(remove).ToList();
                if (players.Count <= 1)
                {
                    if (lastBidder != null)
                    {
                        CurrentInput.Notify(string.Format(Strings.OtherWonAuction, lastBidder));
                        lastBidder.Notify(Strings.YouWonAuction);
                        lastBidder.TransferOwnership(property);
                    }
                    else
                    {
                        CurrentInput.Notify(Strings.AuctionEndNoBids);
                    }
                    break;
                }
            }
        }

        /// <summary>
        ///     Saves this game to a stream.
        /// </summary>
        /// <param name="stream">The stream to save to.</param>
        public void Save(Stream stream)
        {
            //todo
        }

        /// <summary>
        ///     Loads this game from a stream.
        /// </summary>
        /// <param name="stream">The stream to load from.</param>
        /// <returns>The loaded game.</returns>
        public static Game Load(Stream stream) => null;
    }
}
