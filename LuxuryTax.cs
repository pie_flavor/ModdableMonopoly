﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The Luxury Tax square.
    /// </summary>
    [Serializable]
    public class LuxuryTax : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.LuxuryTax;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        /// <inheritdoc />
        public override void HandleLand(Player player)
        {
            player.Notify(Strings.LuxuryTaxMessage);
            player.Pay(75);
        }
    }
}
