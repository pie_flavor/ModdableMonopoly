﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     Represents a color of a <see cref="ColorProperty" />.
    /// </summary>
    [Serializable]
    public class ColorGroup : ISerializable
    {
        /// <summary>
        ///     The purple default color group.
        /// </summary>
        public static readonly ColorGroup Purple = new ColorGroup(50, ColorGroupNames.Purple);

        /// <summary>
        ///     The light blue default color group.
        /// </summary>
        public static readonly ColorGroup LightBlue = new ColorGroup(50, ColorGroupNames.LightBlue);

        /// <summary>
        ///     The pink default color group.
        /// </summary>
        public static readonly ColorGroup Pink = new ColorGroup(100, ColorGroupNames.Pink);

        /// <summary>
        ///     The orange default color group.
        /// </summary>
        public static readonly ColorGroup Orange = new ColorGroup(100, ColorGroupNames.Orange);

        /// <summary>
        ///     The red default color group.
        /// </summary>
        public static readonly ColorGroup Red = new ColorGroup(150, ColorGroupNames.Red);

        /// <summary>
        ///     The yellow default color group.
        /// </summary>
        public static readonly ColorGroup Yellow = new ColorGroup(150, ColorGroupNames.Yellow);

        /// <summary>
        ///     The green default color group.
        /// </summary>
        public static readonly ColorGroup Green = new ColorGroup(200, ColorGroupNames.Green);

        /// <summary>
        ///     The dark blue default color group.
        /// </summary>
        public static readonly ColorGroup DarkBlue = new ColorGroup(200, ColorGroupNames.DarkBlue);

        /// <summary>
        ///     Creates a new color group with the specified house prices.
        /// </summary>
        /// <param name="housePrice">The price of a house.</param>
        /// <param name="name">The name of the group.</param>
        public ColorGroup(int housePrice, [Localizable(true)] string name)
        {
            HousePrice = housePrice;
            Name = name;
        }

        /// <summary>
        ///     The price of houses for properties of this color.
        /// </summary>
        public int HousePrice { get; }

        /// <summary>
        ///     The price a house is sold for on properties of this color.
        /// </summary>
        public int HouseSellPrice => HousePrice / 2;

        [Localizable(true)]
        public string Name { get; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", Name);
            info.AddValue("houseprice", HousePrice);
        }
    }
}
