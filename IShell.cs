﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Monopoly
{
    /// <summary>
    ///     An interface over the input and output between the <see cref="Game" />
    ///     and the players.
    /// </summary>
    public interface IShell
    {
        /// <summary>
        ///     Whether or not the game can currently be saved.
        /// </summary>
        bool CanSave { get; set; }

        /// <summary>
        ///     The game that this shell is currently attached to. Can be null;
        /// </summary>
        Game Game { get; set; }

        /// <summary>
        ///     Expects a general string.
        /// </summary>
        /// <param name="prompt">
        ///     The prompt to use when requesting. Can be null.
        /// </param>
        /// <returns>The result.</returns>
        string Expect([Localizable(true)] string prompt = null);

        /// <summary>
        ///     Expects a string to match a regular expression.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <param name="expected">The pattern to match against.</param>
        /// <param name="silent">
        ///     If true, the prompt is not printed the first time.
        /// </param>
        /// <returns>The result.</returns>
        string Expect([Localizable(true)] string prompt, Regex expected, bool silent = false);

        /// <summary>
        ///     Expects a string to match one or more literal strings.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <param name="expected">The string(s) to match against.</param>
        /// <returns>The result.</returns>
        [return: Localizable(true)]
        string Expect([Localizable(true)] string prompt, [Localizable(true)] params string[] expected);

        /// <summary>
        ///     Expects a string to match one or more literal strings.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <param name="expected">The string(s) to match against.</param>
        /// <returns>The result.</returns>
        [return: Localizable(true)]
        string Expect([Localizable(true)] string prompt, [Localizable(true)] IEnumerable<string> expected);

        /// <summary>
        ///     Expects a string to successfully pass through a parser function.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <param name="transform">
        ///     The function to use to parse the string.
        /// </param>
        /// <param name="silent">
        ///     If true, the prompt is not printed the first
        ///     time.
        /// </param>
        /// <typeparam name="TResult">The type that gets returned.</typeparam>
        /// <returns>The result.</returns>
        TResult Expect<TResult>([Localizable(true)] string prompt, OutFunc<string, TResult> transform,
            bool silent = false);

        /// <summary>
        ///     Expects a string to match the string representation of one of
        ///     several objects.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <param name="expected">
        ///     The function to use to get the string representation.
        /// </param>
        /// <param name="mapper"></param>
        /// <param name="silent"></param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        TResult Expect<TResult>([Localizable(true)] string prompt, IEnumerable<TResult> expected,
            Func<TResult, string> mapper,
            bool silent = false);

        /// <summary>
        ///     Asks a Yes/No question.
        /// </summary>
        /// <param name="prompt">The prompt to use when requesting.</param>
        /// <returns>The result.</returns>
        bool Confirm([Localizable(true)] string prompt);

        /// <summary>
        ///     Writes a message such that it is immediately visible. The character
        ///     | should be used as a newline delimiter.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <seealso cref="Player.Notify" />
        void Write([Localizable(true)] string message);
    }

    /// <summary>
    ///     A version of <see cref="Func{TResult}" /> which follows the TryFoo
    ///     pattern.
    /// </summary>
    /// <remarks>
    ///     This should be used when an error case is expected, rather than
    ///     throwing an exception.
    /// </remarks>
    /// <param name="t">The argument to the function.</param>
    /// <param name="result">The result of the function.</param>
    /// <typeparam name="T">
    ///     The type of the argument to the function.
    /// </typeparam>
    /// <typeparam name="TResult">
    ///     The type of the result of the function.
    /// </typeparam>
    /// <returns>Whether or not the function was successful.</returns>
    public delegate bool OutFunc<in T, TResult>(T t, out TResult result);
}
