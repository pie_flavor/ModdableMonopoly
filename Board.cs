﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The board of the game. Used in determining the cyclical order of the
    ///     <see cref="BoardSquare" />s.
    /// </summary>
    [Serializable]
    public class Board : IEnumerable<BoardSquare>, ISerializable
    {
        private readonly ReadOnlyCollection<BoardSquare> _squares;

        public Board(IEnumerable<BoardSquare> squares)
        {
            _squares = squares.ToList().AsReadOnly();
        }

        /// <summary>
        ///     A list of all squares on the board.
        /// </summary>
        public IReadOnlyList<BoardSquare> Squares => _squares;

        public int Size => _squares.Count;

        /// <summary>
        ///     Gets the board square at the specified index.
        /// </summary>
        /// <remarks>
        ///     This index is looping; if <paramref name="index" /> is greater than
        ///     the board's total length, it will just wrap back around to the
        ///     start.
        /// </remarks>
        /// <param name="index">The index of the square.</param>
        /// <returns>The square at the specified index.</returns>
        public BoardSquare this[int index]
        {
            get
            {
                index %= Squares.Count;
                if (index < 0)
                {
                    index += Squares.Count;
                }

                return Squares[index];
            }
        }

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "IteratorNeverReturns")]
        public IEnumerator<BoardSquare> GetEnumerator()
        {
            while (true)
            {
                foreach (var square in Squares)
                {
                    yield return square;
                }
            }
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("squares", _squares);
        }

        /// <summary>
        ///     Allows you to begin enumerating over board squares starting at the
        ///     position of a specific square.
        /// </summary>
        /// <param name="square">The square to start from.</param>
        /// <returns>An enumerable with this behavior.</returns>
        public IEnumerable<BoardSquare> StartingFrom(BoardSquare square) => StartingFrom(_squares.IndexOf(square));

        /// <summary>
        ///     Allows you to begin enumerating over board squares starting at a
        ///     specific type of square.
        /// </summary>
        /// <typeparam name="T">The type of square to start from.</typeparam>
        /// <returns>An enumerable with this behavior.</returns>
        public IEnumerable<BoardSquare> StartingFrom<T>() => StartingFrom(Squares.First(s => s is T));

        /// <summary>
        ///     Allows you to begin enumerating over board squares starting at a
        ///     specific type of square.
        /// </summary>
        /// <param name="type">The type of square to start from.</param>
        /// <returns>An enumerable with this behavior.</returns>
        public IEnumerable<BoardSquare> StartingFrom(Type type) =>
            StartingFrom(Squares.First(s => s.GetType() == type));

        /// <summary>
        ///     Allows you to begin enumerating over board squares starting at a
        ///     specific square index.
        /// </summary>
        /// <remarks>
        ///     This index is looping; if <paramref name="square" /> is greater
        ///     than the board's total length, it will just wrap back around to the
        ///     start.
        /// </remarks>
        /// <param name="square">The square to start from.</param>
        /// <returns>An enumerable with this behavior.</returns>
        [SuppressMessage("ReSharper", "GenericEnumeratorNotDisposed")]
        public IEnumerable<BoardSquare> StartingFrom(int square)
        {
            square %= Squares.Count;
            if (square < 0)
            {
                square += Squares.Count;
            }

            var enumerator = GetEnumerator();
            for (int i = 0; i <= square; i++)
            {
                enumerator.MoveNext();
            }

            return enumerator.ToEnumerable();
        }

        /// <summary>
        ///     Gets the index of a particular square.
        /// </summary>
        /// <param name="square">The square to get the index of.</param>
        /// <returns>The index of the square.</returns>
        public int IndexOf(BoardSquare square) => _squares.IndexOf(square);

        /// <summary>
        ///     The sequence of default board squares.
        /// </summary>
        /// <returns>The default board squares.</returns>
        public static List<BoardSquare> DefaultBoard() => new List<BoardSquare>
        {
            new Go(),
            ColorProperty.MediterraneanAve,
            new CommunityChest(),
            ColorProperty.BalticAve,
            new IncomeTax(),
            Railroad.ReadingRailroad,
            ColorProperty.OrientalAve,
            new Chance(),
            ColorProperty.VermontAve,
            ColorProperty.ConnecticutAve,

            new Jail(),
            ColorProperty.StCharlesPlace,
            Utility.ElectricCompany,
            ColorProperty.StatesAve,
            ColorProperty.VirginiaAve,
            Railroad.PennsylvaniaRailroad,
            ColorProperty.StJamesPlace,
            new CommunityChest(),
            ColorProperty.TennesseeAve,
            ColorProperty.NewYorkAve,

            new FreeParking(),
            ColorProperty.KentuckyAve,
            new Chance(),
            ColorProperty.IndianaAve,
            ColorProperty.IllinoisAve,
            Railroad.BAndORailroad,
            ColorProperty.AtlanticAve,
            ColorProperty.VentnorAve,
            Utility.WaterWorks,
            ColorProperty.MarvinGardens,

            new GoToJail(),
            ColorProperty.PacificAve,
            ColorProperty.NorthCarolinaAve,
            new CommunityChest(),
            ColorProperty.PennsylvaniaAve,
            Railroad.ShortLine,
            new Chance(),
            ColorProperty.ParkPlace,
            new LuxuryTax(),
            ColorProperty.Boardwalk
        };
    }
}
