﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Monopoly
{
    /// <summary>
    ///     Represents a railroad property.
    /// </summary>
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [Serializable]
    public class Railroad : Property
    {
        /// <summary>
        ///     The default railroad Reading Railroad.
        /// </summary>
        public static Railroad ReadingRailroad = new Railroad(BoardSquareNames.ReadingRailroad);

        /// <summary>
        ///     The default railroad Pennsylvania Railroad.
        /// </summary>
        public static Railroad PennsylvaniaRailroad = new Railroad(BoardSquareNames.PennsylvaniaRailroad);

        /// <summary>
        ///     The default railroad B&amp;O Railroad.
        /// </summary>
        public static Railroad BAndORailroad = new Railroad(BoardSquareNames.BAndORailroad);

        /// <summary>
        ///     The default railroad Short Line.
        /// </summary>
        public static Railroad ShortLine = new Railroad(BoardSquareNames.ShortLine);

        /// <summary>
        ///     Creates a new railroad property with the specified name.
        /// </summary>
        /// <param name="name">The name of the railroad.</param>
        public Railroad([Localizable(true)] string name)
        {
            Name = name;
        }

        /// <inheritdoc />
        public override string Name { get; }

        /// <inheritdoc />
        public override int Price => 200;

        /// <summary>
        ///     A list of all default railroads.
        /// </summary>
        public new static List<Railroad> AllDefault() => new List<Railroad>
        {
            ReadingRailroad,
            PennsylvaniaRailroad,
            BAndORailroad,
            ShortLine
        };

        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", Name);
        }

        /// <inheritdoc />
        public override int GetRent(Game game)
        {
            var state = game[this] ?? throw new UnownedPropertyException();
            int owned = state.Owner.Properties.Count(p => p is Railroad);
            switch (owned)
            {
                case 0: throw new UnownedPropertyException();
                case 1: return 25;
                case 2: return 50;
                case 3: return 100;
                default: return 200;
            }
        }

        public override string GetInfo(Game game)
        {
            var state = game[this];
            var builder = new StringBuilder();
            if (state == null)
            {
                builder.AppendLine(string.Format(PropertyInfo.Price, Price));
            }
            else
            {
                builder.AppendLine(string.Format(PropertyInfo.Owner, state.Owner));
                builder.AppendLine(string.Format(PropertyInfo.Mortgaged, state.IsMortgaged));
            }
            builder.AppendLine(PropertyInfo.Railroad1);
            builder.AppendLine(PropertyInfo.Railroad2);
            builder.AppendLine(PropertyInfo.Railroad3);
            builder.AppendLine(PropertyInfo.Railroad4);
            if (state != null)
            {
                builder.AppendLine(string.Format(PropertyInfo.EffectiveRent, GetRent(game)));
            }
            return builder.ToString();
        }
    }
}
