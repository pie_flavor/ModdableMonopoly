﻿using System;
using System.Collections.Generic;

namespace Monopoly
{
    public class AdvancePlayerEventArgs : EventArgs
    {
        public Player Player { get; }
        public BoardSquare StartingFrom { get; }
        public BoardSquare EndingUp { get; set; }
        public bool PassGo { get; set; }
        public bool HandleLand { get; set; }
        public bool Cancelled { get; set; }

        public AdvancePlayerEventArgs(Player player, BoardSquare startingFrom, BoardSquare endingUp, bool passGo,
            bool handleLand, bool cancelled)
        {
            Player = player;
            StartingFrom = startingFrom;
            EndingUp = endingUp;
            PassGo = passGo;
            HandleLand = handleLand;
            Cancelled = cancelled;
        }
    }

    public class EndRollEventArgs : EventArgs
    {
        public Player Player { get; }

        public EndRollEventArgs(Player player)
        {
            Player = player;
        }
    }

    public class EndTurnEventArgs : EventArgs
    {
        public Player Player { get; }

        public EndTurnEventArgs(Player player)
        {
            Player = player;
        }
    }

    public class HousePurchasedEventArgs : EventArgs
    {
        public Player Player { get; }
        public ColorPropertyState State { get; }
        public bool Cancelled { get; set; }

        public HousePurchasedEventArgs(Player player, ColorPropertyState state, bool cancelled)
        {
            Player = player;
            State = state;
            Cancelled = cancelled;
        }
    }

    public class HouseSoldEventArgs : EventArgs
    {
        public Player Player { get; }
        public ColorPropertyState State { get; }
        public bool Cancelled { get; set; }

        public HouseSoldEventArgs(Player player, ColorPropertyState state, bool cancelled)
        {
            Player = player;
            State = state;
            Cancelled = cancelled;
        }
    }

    public class JailRollEventArgs : EventArgs
    {
        public Player Player { get; }
        public bool Cancelled { get; set; }

        public JailRollEventArgs(Player player, bool cancelled)
        {
            Player = player;
            Cancelled = cancelled;
        }
    }

    public class MovePlayerEventArgs : EventArgs
    {
        public Player Player { get; }
        public BoardSquare StartingFrom { get; }
        public BoardSquare EndingUp { get; set; }
        public bool PassGo { get; set; }
        public bool HandleLand { get; set; }
        public bool Cancelled { get; set; }

        public MovePlayerEventArgs(Player player, BoardSquare startingFrom, BoardSquare endingUp, bool passGo,
            bool handleLand, bool cancelled)
        {
            Player = player;
            StartingFrom = startingFrom;
            EndingUp = endingUp;
            PassGo = passGo;
            HandleLand = handleLand;
            Cancelled = cancelled;
        }
    }

    public class NewTurnEventArgs : EventArgs
    {
        public new static readonly NewTurnEventArgs Empty = new NewTurnEventArgs();
    }

    public class PlayerCreatedEventArgs : EventArgs
    {
        public Player Player { get; }

        public PlayerCreatedEventArgs(Player player)
        {
            Player = player;
        }
    }

    public class PropertyMortgagedEventArgs : EventArgs
    {
        public Player Player { get; }
        public PropertyState State { get; }
        public bool Cancelled { get; set; }

        public PropertyMortgagedEventArgs(Player player, PropertyState state, bool cancelled)
        {
            Player = player;
            State = state;
            Cancelled = cancelled;
        }
    }

    public class PropertyUnmortgagedEventArgs : EventArgs
    {
        public Player Player { get; }
        public PropertyState State { get; }
        public bool Cancelled { get; set; }

        public PropertyUnmortgagedEventArgs(Player player, PropertyState state, bool cancelled)
        {
            Player = player;
            State = state;
            Cancelled = cancelled;
        }
    }

    public class RolledEventArgs : EventArgs
    {
        public int Die1 { get; set; }
        public int Die2 { get; set; }
        public bool Turn { get; }
        public bool SendMessage { get; set; }

        public RolledEventArgs(int die1, int die2, bool turn, bool sendMessage)
        {
            Die1 = die1;
            Die2 = die2;
            Turn = turn;
            SendMessage = sendMessage;
        }
    }

    public class BankruptedEventArgs : EventArgs
    {
        public Player To { get; }

        public BankruptedEventArgs(Player to)
        {
            To = to;
        }
    }

    public class JailedEventArgs : EventArgs
    {
        public new static JailedEventArgs Empty = new JailedEventArgs();
    }

    public class PropertyPurchasedEventArgs : EventArgs
    {
        public Property Property { get; }
        public bool Cancelled { get; set; }

        public PropertyPurchasedEventArgs(Property property, bool cancelled)
        {
            Property = property;
            Cancelled = cancelled;
        }
    }

    public class PropertyTransferredEventArgs : EventArgs
    {
        public Property Property { get; }
        public PropertyState State { get; set; }
        public bool Cancelled { get; set; }
        public bool HandleMortgage { get; set; }

        public PropertyTransferredEventArgs(Property property, PropertyState state, bool cancelled, bool handleMortgage)
        {
            Property = property;
            State = state;
            Cancelled = cancelled;
            HandleMortgage = handleMortgage;
        }
    }

    public class GameCreatedEventArgs : EventArgs
    {
        public Game Game { get; }

        public GameCreatedEventArgs(Game game)
        {
            Game = game;
        }
    }

    public class GameCreateEventArgs : EventArgs
    {
        public IShell Shell { get; }
        public List<BoardSquare> Squares { get; }
        public List<Card> Chance { get; }
        public List<Card> CommunityChest { get; }

        public GameCreateEventArgs(IShell shell, List<BoardSquare> squares, List<Card> chance,
            List<Card> communityChest)
        {
            Shell = shell;
            Squares = squares;
            Chance = chance;
            CommunityChest = communityChest;
        }
    }

    public class GameLoadedEventArgs : EventArgs
    {
        public Game Game { get; }

        public GameLoadedEventArgs(Game game)
        {
            Game = game;
        }
    }
}
