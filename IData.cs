﻿using System.Runtime.Serialization;

namespace Monopoly
{
    public interface IData : ISerializable { }
}
