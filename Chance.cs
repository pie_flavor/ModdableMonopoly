﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The Chance card square.
    /// </summary>
    [Serializable]
    public class Chance : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.Chance;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        /// <inheritdoc />
        public override void HandleLand(Player player)
        {
            var card = player.Game.DrawChance();
            player.Notify(card.Message);
            card.Action(player);
        }
    }
}
