﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The Community Chest card square.
    /// </summary>
    [Serializable]
    public class CommunityChest : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.CommunityChest;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        /// <inheritdoc />
        public override void HandleLand(Player player)
        {
            var card = player.Game.DrawCommunityChest();
            player.Notify(card.Message);
            card.Action(player);
        }
    }
}
