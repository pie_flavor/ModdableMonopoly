﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    [Serializable]
    public class GoToJail : BoardSquare
    {
        public override string Name => BoardSquareNames.GoToJail;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        public override void HandleLand(Player player)
        {
            player.InJail = true;
        }
    }
}
