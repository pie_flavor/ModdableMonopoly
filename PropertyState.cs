﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The state of an <see cref="Monopoly.Property" /> during gameplay.
    /// </summary>
    [Serializable]
    public class PropertyState : ISerializable
    {
        /// <summary>
        ///     Creates a new property state with the specified property and owner.
        /// </summary>
        /// <param name="property">The represented property.</param>
        /// <param name="owner">The owner of the property.</param>
        public PropertyState(Property property, Player owner)
        {
            Property = property;
            Owner = owner;
        }

        /// <summary>
        ///     Whether this property is mortgaged.
        /// </summary>
        public bool IsMortgaged { get; set; } = false;

        /// <summary>
        ///     Which <see cref="Monopoly.Property" /> this property represents.
        /// </summary>
        public Property Property { get; }

        /// <summary>
        ///     The owner of this property.
        /// </summary>
        public Player Owner { get; set; }

        /// <summary>
        ///     The game that this property resides in.
        /// </summary>
        public Game Game => Owner.Game;

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }

        /// <summary>
        ///     Returns the value of a <see cref="Monopoly.Property.ToString" /> call
        ///     on <see cref="Property" />.
        /// </summary>
        /// <returns>
        ///     Whatever <see cref="Property" />'s
        ///     <see cref="Monopoly.Property.ToString" /> call returns
        /// </returns>
        public override string ToString() => Property.ToString();

        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("property", Property.Name);
            info.AddValue("ismortgaged", IsMortgaged);
            info.AddValue("owner", Owner.Name);
        }
    }
}
