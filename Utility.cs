﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Monopoly
{
    /// <summary>
    ///     Represents a utility property.
    /// </summary>
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [Serializable]
    public class Utility : Property
    {
        /// <summary>
        ///     The default utility Electric Company.
        /// </summary>
        public static Utility ElectricCompany = new Utility(BoardSquareNames.ElectricCompany);

        /// <summary>
        ///     The default utility Water Works.
        /// </summary>
        public static Utility WaterWorks = new Utility(BoardSquareNames.WaterWorks);

        /// <summary>
        ///     Creates a new utility with the specified name.
        /// </summary>
        /// <param name="name">The name of the utility.</param>
        public Utility([Localizable(true)] string name)
        {
            Name = name;
        }

        /// <inheritdoc />
        public override string Name { get; }

        /// <inheritdoc />
        public override int Price => 150;

        /// <summary>
        ///     A list of all default utilities.
        /// </summary>
        public new static List<Utility> AllDefault() => new List<Utility>
        {
            ElectricCompany,
            WaterWorks
        };

        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", Name);
        }

        /// <inheritdoc />
        public override int GetRent(Game game)
        {
            var state = game[this] ?? throw new UnownedPropertyException();
            if (state.Owner.Properties.Count(p => p is Utility) > 1)
            {
                return 10 * game.LastDieRoll;
            }
            else
            {
                return 4 * game.LastDieRoll;
            }
        }

        public override string GetInfo(Game game)
        {
            var state = game[this];
            var builder = new StringBuilder();
            if (state == null)
            {
                builder.AppendLine(string.Format(PropertyInfo.Price, Price));
            }
            else
            {
                builder.AppendLine(string.Format(PropertyInfo.Owner, state.Owner));
                builder.AppendLine(string.Format(PropertyInfo.Mortgaged, state.IsMortgaged));
            }
            builder.AppendLine(PropertyInfo.Utility1);
            builder.AppendLine(PropertyInfo.Utility2);
            if (state != null)
            {
                int factor = state.Owner.Properties.WhereCast<Utility>().Count() == 1 ? 4 : 10;
                builder.AppendLine(string.Format(PropertyInfo.UtilityEffectiveRent, factor));
            }
            return builder.ToString();
        }
    }
}
