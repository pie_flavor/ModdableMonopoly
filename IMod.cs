﻿using System.ComponentModel;

namespace Monopoly
{
    public interface IMod
    {
        string Id { get; }

        [Localizable(true)]
        string Name { get; }

        Card GetCard(string name);
    }
}
