﻿using System;
using System.Runtime.Serialization;
using Monopoly;

namespace MegaEdition
{
    [Serializable]
    public class MegaPlayerData : IData
    {
        private readonly MegaEditionBase _mod;
        private readonly Player _player;
        
        public MegaPlayerData(MegaEditionBase mod, Player player)
        {
            _mod = mod;
            _player = player;
        }
        
        public int BusTickets { get; set; }

        private readonly Random _random = new Random();
        
        public void DrawBusTicket()
        {
            var gdata = (MegaGameData)_player.Game[_mod];
            if (gdata.BusTickets == 0 && gdata.DiscardBusTickets == 0)
            {
                _player.Notify(Strings.NoRemainingTickets);
                return;
            }
            int i = _random.Next(gdata.BusTickets + gdata.DiscardBusTickets);
            if (i < gdata.BusTickets && gdata.BusTickets != 0)
            {
                gdata.BusTickets--;
                BusTickets++;
            }
            else
            {
                gdata.DiscardBusTickets--;
                BusTickets = 1;
                foreach (var player in _player.Game.Players.Except(_player))
                {
                    var pdata = (MegaPlayerData)player[_mod];
                    pdata.BusTickets = 0;
                    player.Notify(string.Format(Strings.DiscardTicketOther, _player));
                }
                _player.Notify(Strings.DiscardTicketSelf);
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("bustickets", BusTickets);
        }
    }
}
