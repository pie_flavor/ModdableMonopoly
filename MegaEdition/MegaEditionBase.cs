﻿using System.Diagnostics.CodeAnalysis;
using Monopoly;

namespace MegaEdition
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class MegaEditionBase : IMod
    {   
        public const string ModId = "MegaEdition";
        private readonly SpeedDie _die;
        
        public MegaEditionBase()
        {
            Program.GameCreate += TweakGame;
            Program.GameCreated += AddHandlers;
            MegaColorProperty.InstallProperties();
            MegaUtility.InstallProperties();
            _die = new SpeedDie(this);
        }

        private void AddHandlers(object sender, GameCreatedEventArgs args)
        {
            args.Game[this] = new MegaGameData();
            args.Game.PlayerCreated += TweakPlayer;
            _die.AddHandlers(args.Game);
        }

        private void TweakPlayer(Game game, PlayerCreatedEventArgs args)
        {
            var player = args.Player;
            player.Money = 2500;
            player[this] = new MegaPlayerData(this, player);
        }

        private void TweakGame(object sender, GameCreateEventArgs args)
        {
            var squares = args.Squares;
            squares.Insert(squares.IndexOf(ColorProperty.BalticAve) + 1, MegaColorProperty.ArcticAve);
            squares.Insert(squares.IndexOf(ColorProperty.OrientalAve), MegaColorProperty.MassachusettsAve);
            squares.Insert(squares.IndexOf(ColorProperty.VermontAve), MegaUtility.GasCompany);
            squares.Insert(squares.IndexOf(ColorProperty.StCharlesPlace), new Auction());
            squares.Insert(squares.IndexOf(ColorProperty.StCharlesPlace), MegaColorProperty.MarylandAve);
            squares.Insert(squares.IndexOf(ColorProperty.NewYorkAve) + 1, MegaColorProperty.NewJerseyAve);
            squares.Insert(squares.IndexOf(ColorProperty.IllinoisAve) + 1, MegaColorProperty.MichiganAve);
            squares.Insert(squares.IndexOf(Railroad.BAndORailroad), new BusTicket());
            squares.Insert(squares.IndexOf(ColorProperty.MarvinGardens) + 1, MegaColorProperty.CaliforniaAve);
            squares.Insert(squares.IndexOf(ColorProperty.NorthCarolinaAve), MegaColorProperty.SouthCarolinaAve);
            squares.Insert(squares.IndexOf(ColorProperty.ParkPlace), new BirthdayGift());
            squares.Insert(squares.IndexOf(ColorProperty.ParkPlace), MegaColorProperty.FloridaAve);

            for (int i = 0; i < squares.Count; i++)
            {
                var square = squares[i];
                if (square is ColorProperty prop && !(square is MegaColorProperty))
                {
                    squares[i] = new MegaColorProperty(prop);
                }
                if (square is Utility utility && !(square is MegaUtility))
                {
                    squares[i] = new MegaUtility(utility);
                }
            }
        }

        public string Id => ModId;
        public string Name => Strings.ModName;
        
        public Card GetCard(string name) => null;
    }
}
