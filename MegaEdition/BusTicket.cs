﻿using System;
using System.Runtime.Serialization;
using Monopoly;

namespace MegaEdition
{
    [Serializable]
    public class BusTicket : BoardSquare
    {
        public override void HandleLand(Player player)
        {
            var pdata = (MegaPlayerData)player[MegaEditionBase.ModId];
            pdata.DrawBusTicket();
        }

        public override string Name => BoardSquareNames.BusTicket;
        
        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }
    }
}
