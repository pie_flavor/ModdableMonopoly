﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Monopoly;

namespace MegaEdition
{
    [Serializable]
    public class Auction : BoardSquare
    {
        public override void HandleLand(Player player)
        {
            player.Game.CurrentInput = player;
            var unowned = player.Game.Properties.Where(p => player.Game[p] == null).ToArray();
            if (unowned.Any())
            {
                var prop = player.Game.Shell.Expect(Strings.EnterUnownedProperty, unowned, p => p.Name);
                player.Game.AuctionProperty(prop);
            }
            else
            {
                var props = player.Game.Properties.Select(p => (Property: p, Rent: p.GetRent(player.Game)))
                    .OrderByDescending(t => t.Rent).ToArray();
                if (props[1].Rent == props[0].Rent)
                {
                    props = props.Where(p => p.Rent == props[0].Rent)
                        .OrderBy(p => player.Game.Board.IndexOf(p.Property)).ToArray();
                }
                player.Game.SetSquare(player, props[0].Property);
            }
            player.Game.CurrentInput = null;
        }

        public override string Name => BoardSquareNames.Auction;
        
        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }
    }
}
