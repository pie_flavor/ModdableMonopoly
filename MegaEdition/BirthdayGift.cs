﻿using System;
using System.Runtime.Serialization;
using Monopoly;

namespace MegaEdition
{
    [Serializable]
    public class BirthdayGift : BoardSquare
    {
        public override string Name => BoardSquareNames.BirthdayGift;
        
        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        public override void HandleLand(Player player)
        {
            player.Game.CurrentInput = player;
            var gdata = (MegaGameData)player.Game[MegaEditionBase.ModId];
            if (gdata.BusTickets + gdata.DiscardBusTickets == 0 ||
                player.Game.Shell.Expect(Strings.BirthdayGiftChoice, Strings.BirthdayGiftChoiceCollect,
                    Strings.BirthdayGiftChoiceDraw) == Strings.BirthdayGiftChoiceCollect)
            {
                player.Money += 100;
            }
            else
            {
                var pdata = (MegaPlayerData)player[MegaEditionBase.ModId];
                pdata.DrawBusTicket();
            }
            player.Game.CurrentInput = null;
        }
    }
}
