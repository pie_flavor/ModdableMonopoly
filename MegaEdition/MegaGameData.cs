﻿using System;
using System.Runtime.Serialization;
using Monopoly;

namespace MegaEdition
{
    [Serializable]
    public class MegaGameData : IData
    {
        public int BusTickets { get; set; } = 13;
        public int DiscardBusTickets { get; set; } = 3;

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("bustickets", BusTickets);
            info.AddValue("discardbustickets", DiscardBusTickets);
        }
    }
}
