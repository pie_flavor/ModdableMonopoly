﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Monopoly;
using MonopolyPropertyInfo = Monopoly.PropertyInfo;

namespace MegaEdition
{
    [Serializable]
    public class MegaColorProperty : ColorProperty
    {
        public static readonly MegaColorProperty ArcticAve =
            new MegaColorProperty(80, 5, 30, 80, 240, 360, 450, 950, ColorGroup.Purple, BoardSquareNames.ArcticAve);

        public static readonly MegaColorProperty MassachusettsAve =
            new MegaColorProperty(100, 6, 30, 90, 270, 400, 550, 1050, ColorGroup.LightBlue,
                BoardSquareNames.MassachusettsAve);

        public static readonly MegaColorProperty MarylandAve =
            new MegaColorProperty(140, 10, 50, 150, 450, 625, 750, 1250, ColorGroup.Pink, BoardSquareNames.MarylandAve);

        public static readonly MegaColorProperty NewJerseyAve =
            new MegaColorProperty(200, 16, 80, 220, 600, 800, 1000, 1500, ColorGroup.Orange,
                BoardSquareNames.NewJerseyAve);

        public static readonly MegaColorProperty MichiganAve =
            new MegaColorProperty(240, 20, 100, 300, 750, 925, 1100, 2100, ColorGroup.Red,
                BoardSquareNames.MichiganAve);

        public static readonly MegaColorProperty CaliforniaAve =
            new MegaColorProperty(280, 24, 120, 360, 850, 1025, 1200, 2200, ColorGroup.Yellow,
                BoardSquareNames.CaliforniaAve);

        public static readonly MegaColorProperty SouthCarolinaAve =
            new MegaColorProperty(300, 26, 130, 390, 900, 1100, 1275, 2275, ColorGroup.Green,
                BoardSquareNames.SouthCarolinaAve);

        public static readonly MegaColorProperty FloridaAve =
            new MegaColorProperty(350, 35, 175, 500, 1100, 1300, 1500, 2500, ColorGroup.DarkBlue,
                BoardSquareNames.FloridaAve);

        public int SkyscraperRent { get; }

        public MegaColorProperty(int price, int rent, int house1Rent, int house2Rent,
            int house3Rent, int house4Rent, int hotelRent, int skyscraperRent, ColorGroup colorGroup,
            [Localizable(true)] string name) :
            base(price, rent, house1Rent, house2Rent, house3Rent, house4Rent, hotelRent, colorGroup, name)
        {
            SkyscraperRent = skyscraperRent;
        }

        public MegaColorProperty(ColorProperty property, int skyscraperRent) : this(
            property.Price, property.Rent, property.House1Rent, property.House2Rent,
            property.House3Rent, property.House4Rent, property.HotelRent, skyscraperRent, property.ColorGroup,
            property.Name) { }

        public MegaColorProperty(ColorProperty property) :
            this(property, property.HotelRent + (property.ColorGroup.HousePrice <= 100 ? 500 : 1000)) { }

        public override int GetRent(Game game)
        {
            var state = game[this] ?? throw new UnownedPropertyException();
            int totalProperties = game.Properties.WhereCast<ColorProperty>()
                .Count(c => c.ColorGroup == ColorGroup);
            int owned = state.Owner.Properties.WhereCast<ColorProperty>().Count(c => c.ColorGroup == ColorGroup);
            switch (state.Houses)
            {
                case 0: return totalProperties == owned ? Rent * 3 : totalProperties / 2 >= owned ? Rent * 2 : Rent;
                case 1: return House1Rent;
                case 2: return House2Rent;
                case 3: return House3Rent;
                case 4: return House4Rent;
                case 5: return HotelRent;
                case 6: return totalProperties == owned ? SkyscraperRent : HotelRent;
                default: throw new InvalidPropertyStateException($"{nameof(state.Houses)} = {state.Houses}");
            }
        }

        public override string GetInfo(Game game)
        {
            var state = game[this];
            var builder = new StringBuilder();
            if (state == null)
            {
                builder.AppendLine(string.Format(MonopolyPropertyInfo.Price, Price));
            }
            else
            {
                builder.AppendLine(string.Format(MonopolyPropertyInfo.Owner, state.Owner));
                builder.AppendLine(string.Format(MonopolyPropertyInfo.Mortgaged, state.IsMortgaged));
                builder.AppendLine(state.Houses == 5 ? MonopolyPropertyInfo.ColorHotelStatus :
                    state.Houses == 6 ? PropertyInfo.ColorSkyscraperStatus :
                    string.Format(MonopolyPropertyInfo.ColorHouseCount, state.Houses));
            }
            builder.AppendLine(string.Format(MonopolyPropertyInfo.ColorBaseRent, Rent));
            builder.AppendLine(string.Format(MonopolyPropertyInfo.Color1House, House1Rent));
            builder.AppendLine(string.Format(MonopolyPropertyInfo.Color2Houses, House2Rent));
            builder.AppendLine(string.Format(MonopolyPropertyInfo.Color3Houses, House3Rent));
            builder.AppendLine(string.Format(MonopolyPropertyInfo.Color4Houses, House4Rent));
            builder.AppendLine(string.Format(MonopolyPropertyInfo.ColorHotel, HotelRent));
            if (state != null)
            {
                builder.AppendLine(string.Format(MonopolyPropertyInfo.EffectiveRent, GetRent(state.Game)));
            }
            return builder.ToString();
        }

        public override int MaxHouses => 6;

        public static void InstallProperties()
        {
            MediterraneanAve = new MegaColorProperty(MediterraneanAve);
            BalticAve = new MegaColorProperty(BalticAve);
            OrientalAve = new MegaColorProperty(OrientalAve);
            VermontAve = new MegaColorProperty(VermontAve);
            ConnecticutAve = new MegaColorProperty(ConnecticutAve);
            StCharlesPlace = new MegaColorProperty(StCharlesPlace);
            StatesAve = new MegaColorProperty(StatesAve);
            VirginiaAve = new MegaColorProperty(VirginiaAve);
            StJamesPlace = new MegaColorProperty(StJamesPlace);
            TennesseeAve = new MegaColorProperty(TennesseeAve);
            NewYorkAve = new MegaColorProperty(NewYorkAve);
            KentuckyAve = new MegaColorProperty(KentuckyAve);
            IndianaAve = new MegaColorProperty(IndianaAve);
            IllinoisAve = new MegaColorProperty(IllinoisAve);
            AtlanticAve = new MegaColorProperty(AtlanticAve);
            VentnorAve = new MegaColorProperty(VentnorAve);
            MarvinGardens = new MegaColorProperty(MarvinGardens);
            PacificAve = new MegaColorProperty(PacificAve);
            NorthCarolinaAve = new MegaColorProperty(NorthCarolinaAve);
            PennsylvaniaAve = new MegaColorProperty(PennsylvaniaAve);
            ParkPlace = new MegaColorProperty(ParkPlace);
            Boardwalk = new MegaColorProperty(Boardwalk);
        }

        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("skyscraperrent", SkyscraperRent);
        }
    }
}
