﻿using System;
using System.Linq;
using Monopoly;

namespace MegaEdition
{
    public class SpeedDie
    {
        private readonly MegaEditionBase _mod;

        public SpeedDie(MegaEditionBase mod)
        {
            _mod = mod;
        }

        public void AddHandlers(Game game)
        {
            game.EndRoll += OnEndRoll;
            game.Rolled += OnRolled;
            game.AdvancePlayer += OnAdvancePlayer;
        }

        private readonly Random _die = new Random();

        private object _mrMonopolyToken = null;

        private object _triplesToken = null;

        private void OnRolled(Game game, RolledEventArgs args)
        {
            if (args.Turn)
            {
                int roll = _die.Next(1, 7);
                switch (roll)
                {
                    case 1:
                    case 2:
                    case 3:
                        args.SendMessage = false;
                        game.CurrentTurn.Notify(string.Format(Strings.DiceRoll, args.Die1, args.Die2, roll));
                        if (args.Die1 == args.Die2 && args.Die1 == roll)
                        {
                            _triplesToken = game.TurnToken;
                            args.Die2 += roll;
                        }
                        else
                        {
                            if (args.Die1 > args.Die2)
                            {
                                args.Die1 += roll;
                            }
                            else
                            {
                                args.Die2 += roll;
                            }
                        }
                        break;
                    case 4:
                    case 5:
                        game.CurrentTurn.Notify(Strings.DiceRollBus);
                        var pdata = (MegaPlayerData)game.CurrentTurn[_mod];
                        var gdata = (MegaGameData)game[_mod];
                        if (gdata.BusTickets == 0 && gdata.DiscardBusTickets == 0)
                        {
                            game.CurrentTurn.Notify(Strings.NoMoreBusTickets);
                            game.CurrentTurn.Game.SetSquare(game.CurrentTurn,
                                game.CurrentTurn.Game.Board.First(s => s is Chance || s is CommunityChest));
                        }
                        else
                        {
                            pdata.DrawBusTicket();
                        }
                        break;
                    case 6:
                        game.CurrentTurn.Notify(Strings.DiceRollMrMonopoly);
                        _mrMonopolyToken = game.TurnToken;
                        break;
                }
            }
        }

        private void OnEndRoll(Game game, EndRollEventArgs args)
        {
            if (_mrMonopolyToken == game.TurnToken)
            {
                var prop = game.Board.StartingFrom(game[args.Player])
                    .FirstOrDefault(s => s is Property p && game[p] == null);
                if (prop != null)
                {
                    args.Player.Notify(Strings.DiceRollMrMonopolyEffect);
                    game.SetSquare(args.Player, prop);
                }
                else
                {
                    prop = game.Board.StartingFrom(game[args.Player]).FirstOrDefault(s =>
                        s is Property p && !game[p].IsMortgaged && game[p].Owner != args.Player);
                    if (prop != null)
                    {
                        args.Player.Notify(Strings.DiceRollMrMonopolyEffect);
                        game.SetSquare(args.Player, prop);
                    }
                }
            }    
            _mrMonopolyToken = null;
        }

        private void OnAdvancePlayer(Game game, AdvancePlayerEventArgs args)
        {
            if (game.TurnToken == _triplesToken)
            {
                game.CurrentInput = args.Player;
                int square = game.Shell.Expect(Strings.MrMonopolyQuery,
                    (string s, out int i) => int.TryParse(s, out i));
                args.EndingUp = game.Board[square];
            }
            _triplesToken = null;
        }
    }
}
