﻿using System;
using System.ComponentModel;
using Monopoly;
using System.Linq;

namespace MegaEdition
{
    [Serializable]
    public class MegaUtility : Utility
    {
        public static readonly MegaUtility GasCompany = new MegaUtility(BoardSquareNames.GasCompany);

        public MegaUtility([Localizable(true)] string name) : base(name) { }
        
        public MegaUtility(Utility utility) : base(utility.Name) { }

        public override int GetRent(Game game)
        {
            var state = game[this] ?? throw new UnownedPropertyException();
            int utilities = state.Owner.Properties.WhereCast<Utility>().Count();
            if (utilities == 2)
            {
                return 20 * game.LastDieRoll;
            }
            else if (utilities == 1)
            {
                return 10 * game.LastDieRoll;
            }
            else
            {
                return 4 * game.LastDieRoll;
            }
        }

        public static void InstallProperties()
        {
            ElectricCompany = new MegaUtility(ElectricCompany);
            WaterWorks = new MegaUtility(WaterWorks);
        }
    }
}
