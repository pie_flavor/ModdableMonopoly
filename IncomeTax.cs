﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The income tax square.
    /// </summary>
    [Serializable]
    public class IncomeTax : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.IncomeTax;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        /// <inheritdoc />
        public override void HandleLand(Player player)
        {
            player.Game.CurrentInput = player;
            if (player.Game.Shell.Expect(Strings.IncomeTaxChoice,
                    Strings.IncomeTaxChoiceFlat, Strings.IncomeTaxChoiceTotal) == Strings.IncomeTaxChoiceTotal)
            {
                player.Pay(200);
            }
            else
            {
                int totalValue = player.Money + player.Properties.Select(p =>
                                     p.Price + (p is ColorProperty cp
                                         ? player.Game[cp].Houses * cp.ColorGroup.HousePrice
                                         : 0)).Sum() / 10;
                player.Pay(totalValue);
                player.Notify(string.Format(Strings.PaidTaxes, totalValue));
            }
            player.Game.CurrentInput = null;
        }
    }
}
