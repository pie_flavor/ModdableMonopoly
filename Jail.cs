﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The Jail square. Does nothing ('just visiting').
    /// </summary>
    [Serializable]
    public class Jail : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.Jail;

        /// <inheritdoc />
        public override void HandleLand(Player player) { }

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }
    }
}
