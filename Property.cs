﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Monopoly
{
    /// <summary>
    ///     Some sort of purchasable property.
    /// </summary>
    public abstract class Property : BoardSquare
    {
        /// <summary>
        ///     All the default properties.
        /// </summary>
        public static readonly IReadOnlyList<Property> AllDefault;

        static Property()
        {
            var list = new List<Property>();
            list.AddRange(ColorProperty.AllDefault());
            list.AddRange(Utility.AllDefault());
            list.AddRange(Railroad.AllDefault());
            AllDefault = list.AsReadOnly();
        }

        /// <summary>
        ///     Gets the name of this property.
        /// </summary>
        public abstract override string Name { get; }

        /// <summary>
        ///     Gets the price of this property.
        /// </summary>
        public abstract int Price { get; }

        /// <summary>
        ///     The mortgage value of this property.
        /// </summary>
        public virtual int MortgageValue => Price / 2;

        /// <summary>
        ///     The price to unmortgage this property.
        /// </summary>
        public virtual int UnmortgageValue => (int)(MortgageValue * 1.1);

        /// <summary>
        ///     The interest that must be paid upon transferring this property.
        /// </summary>
        public virtual int UnmortgageInterest => (int)(MortgageValue * 0.1);

        /// <summary>
        ///     Gets the rent this property accrues upon landing.
        /// </summary>
        /// <param name="game">The game this resides in.</param>
        /// <returns>The rent.</returns>
        public abstract int GetRent(Game game);

        /// <inheritdoc />
        public override void HandleLand(Player player)
        {
            if (player.Properties.Contains(this))
            {
                player.Notify(Strings.OwnProperty);
                return;
            }

            var state = player.Game[this];
            if (state == null)
            {
                player.Notify(string.Format(Strings.UnownedProperty, Price));
                player.RequestPurchase(this);
            }
            else
            {
                var owner = state.Owner;
                int money = GetRent(state.Game);
                player.Notify(string.Format(Strings.RentOwed, owner, money));
                player.Pay(owner, money);
                owner.Notify(string.Format(Strings.RentReceived, player, money));
                player.Notify(string.Format(Strings.RentPaid, money, owner));
            }
        }

        /// <summary>
        ///     Gets information about this property.
        /// </summary>
        /// <param name="game">The game that this resides in.</param>
        /// <returns>Information about this property.</returns>
        [Localizable(true)]
        public abstract string GetInfo(Game game);

        /// <summary>
        ///     Creates a state for this property.
        /// </summary>
        /// <param name="owner">The new owner of this property.</param>
        /// <returns>The created state.</returns>
        /// <remarks>
        ///     The returned state should be passed
        /// </remarks>
        public virtual PropertyState CreateState(Player owner) => new PropertyState(this, owner);
    }
}
