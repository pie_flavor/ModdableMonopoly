﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     Basic interface for squares on the Monopoly board.
    /// </summary>
    [Serializable]
    public abstract class BoardSquare : ISerializable
    {
        /// <summary>
        ///     Gets the name of this square.
        /// </summary>
        /// <returns>The name of this square.</returns>
        [Localizable(true)]
        public abstract string Name { get; }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            GetObjectData(info, context);
        }

        /// <summary>
        ///     Handles a <see cref="Player" /> landing on this square.
        /// </summary>
        /// <param name="player">The player.</param>
        public abstract void HandleLand(Player player);

        public override string ToString() => Name;

        protected abstract void GetObjectData(SerializationInfo info, StreamingContext context);
    }
}
