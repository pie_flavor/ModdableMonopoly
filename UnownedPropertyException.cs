﻿using System;

namespace Monopoly
{
    /// <summary>
    ///     Thrown when an unowned property is passed to a method that expects it
    ///     to be owned.
    /// </summary>
    public class UnownedPropertyException : Exception { }
}
