﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Monopoly
{
    /// <summary>
    ///     Various extensions for things.
    /// </summary>
    public static class Extensions
    {
        private static readonly Random Rng = new Random();

        /// <summary>
        ///     Checks if this <see cref="IEnumerable{T}" /> contains all members
        ///     of another <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <param name="superset">
        ///     The containing <see cref="IEnumerable{T}" />.
        /// </param>
        /// <param name="subset">
        ///     The contained <see cref="IEnumerable{T}" />.
        /// </param>
        /// <typeparam name="T">
        ///     The type of <see cref="IEnumerable{T}" />.
        /// </typeparam>
        /// <returns>
        ///     Whether <paramref name="superset" /> contains every element in
        ///     <paramref name="subset" />.
        /// </returns>
        public static bool ContainsAll<T>(this IEnumerable<T> superset, IEnumerable<T> subset)
            => !subset.Except(superset).Any();

        /// <summary>
        ///     Wraps an <see cref="IEnumerable{T}" /> in an infinitely repeating
        ///     wrapper.
        /// </summary>
        /// <param name="enumerable">
        ///     The <see cref="IEnumerable{T}" /> to wrap.
        /// </param>
        /// <typeparam name="T">
        ///     The type of <see cref="IEnumerable{T}" />.
        /// </typeparam>
        /// <returns>An infinitely repeating wrapper.</returns>
        [SuppressMessage("ReSharper", "IteratorNeverReturns")]
        public static IEnumerable<T> Cycle<T>(this IEnumerable<T> enumerable)
        {
            var ts = enumerable as T[] ?? enumerable.ToArray();
            while (true)
            {
                foreach (var t in ts)
                {
                    yield return t;
                }
            }
        }

        /// <summary>
        ///     Wraps a <see cref="Queue{T}" /> in an <see cref="IEnumerable{T}" />
        ///     which pops from the <see cref="Queue{T}" /> every time the
        ///     <see cref="IEnumerable{T}" /> is advanced.
        /// </summary>
        /// <param name="queue">The <see cref="Queue{T}" /> to wrap.</param>
        /// <typeparam name="T">
        ///     The type of <see cref="Queue{T}" />.
        /// </typeparam>
        /// <returns>A draining wrapper.</returns>
        public static IEnumerable<T> Drain<T>(this Queue<T> queue)
        {
            while (queue.Count > 0)
            {
                yield return queue.Dequeue();
            }
        }

        /// <summary>
        ///     Creates an <see cref="IEnumerable{T}" /> from an
        ///     <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <param name="enumerator">
        ///     The <see cref="IEnumerator{T}" /> to use
        /// </param>
        /// <typeparam name="T">
        ///     The type of <see cref="IEnumerator{T}" />
        /// </typeparam>
        /// <returns>The resulting <see cref="IEnumerable{T}" /></returns>
        public static IEnumerable<T> ToEnumerable<T>(this IEnumerator<T> enumerator)
        {
            using (enumerator)
            {
                while (enumerator.MoveNext())
                {
                    yield return enumerator.Current;
                }
            }
        }

        /// <summary>
        ///     Filters out all elements which are not <typeparamref name="T" />
        ///     from an <see cref="IEnumerable" /> and returns as an
        ///     <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <param name="enumerable">The enumerable to use.</param>
        /// <typeparam name="T">The type parameter to look for.</typeparam>
        /// <returns>The resulting sequence.</returns>
        public static IEnumerable<T> WhereCast<T>(this IEnumerable enumerable)
        {
            foreach (var item in enumerable)
            {
                if (item is T t)
                {
                    yield return t;
                }
            }
        }

        /// <summary>
        ///     A variant of
        ///     <see cref="Enumerable.Except{T}(IEnumerable{T},IEnumerable{T})" />
        ///     that takes a vararg instead of <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <param name="enumerable">The enumerable to wrap.</param>
        /// <param name="elements">
        ///     All elements to skip from <paramref name="enumerable" />.
        /// </param>
        /// <typeparam name="T">The type of enumerable to use.</typeparam>
        /// <returns>The resulting exclusionary enumerable.</returns>
        public static IEnumerable<T> Except<T>(this IEnumerable<T> enumerable, params T[] elements)
            => enumerable.Except((IEnumerable<T>)elements);

        /// <summary>
        ///     Shuffles an <see cref="IEnumerable{T}" />.
        /// </summary>
        /// <param name="enumerable">The enumerable to shuffle.</param>
        /// <typeparam name="T">The type of enumerable to use.</typeparam>
        /// <returns>A shuffled copy.</returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> enumerable)
        {
            var array = enumerable.ToArray();
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = Rng.Next(n + 1);
                var value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
            return array;
        }

        public static IEnumerable<T> Rotate<T>(this IEnumerable<T> enumerable, T start)
        {
            var list = enumerable.ToList();
            int idx = list.IndexOf(start);
            if (idx == -1)
            {
                foreach (var t in list)
                {
                    yield return t;
                }
                yield break;
            }
            int size = list.Count;
            for (int i = idx; i < size; i++)
            {
                yield return list[i];
            }
            for (int i = 0; i < idx; i++)
            {
                yield return list[i];
            }
        }
    }
}
