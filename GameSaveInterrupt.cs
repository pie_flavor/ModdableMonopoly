﻿using System;

namespace Monopoly
{
    /// <summary>
    ///     Thrown by the <see cref="IShell" /> to exit out of a game.
    /// </summary>
    /// <remarks>
    ///     Do not attempt to catch unless you are sure that
    ///     <see cref="Game.Run" /> is not above you in the call stack.
    /// </remarks>
    public class GameSaveInterrupt : Exception { }
}
