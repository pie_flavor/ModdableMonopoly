﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     Represents the state of a <see cref="ColorProperty" /> during a
    ///     <see cref="Game" />.
    /// </summary>
    [Serializable]
    public class ColorPropertyState : PropertyState
    {
        /// <summary>
        ///     Creates a new color property state with the provided property and
        ///     owner.
        /// </summary>
        /// <param name="property">
        ///     The property that this state represents.
        /// </param>
        /// <param name="owner">
        ///     The owner of this property.
        /// </param>
        public ColorPropertyState(ColorProperty property, Player owner) : base(property, owner) { }

        /// <summary>
        ///     The number of houses that this property currently has. A value of 5
        ///     indicates a hotel.
        /// </summary>
        public int Houses { get; set; } = 0;

        public new ColorProperty Property => (ColorProperty)base.Property;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("houses", Houses);
        }
    }
}
