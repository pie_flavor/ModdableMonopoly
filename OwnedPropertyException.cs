﻿using System;

namespace Monopoly
{
    /// <summary>
    ///     Thrown when an owned property is passed to a method that expects it to
    ///     be unowned.
    /// </summary>
    public class OwnedPropertyException : Exception { }
}
