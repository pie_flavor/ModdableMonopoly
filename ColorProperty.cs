﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Monopoly
{
    /// <summary>
    ///     Represents a color-group property.
    /// </summary>
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [Serializable]
    public class ColorProperty : Property
    {
        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Purple" /> color
        ///     property Mediterranean Ave.
        /// </summary>
        public static ColorProperty MediterraneanAve =
            new ColorProperty(60, 2, 10, 30, 90, 160, 250, ColorGroup.Purple, BoardSquareNames.MediterraneanAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Purple" /> color
        ///     property Baltic Ave.
        /// </summary>
        public static ColorProperty BalticAve =
            new ColorProperty(60, 4, 20, 60, 180, 320, 450, ColorGroup.Purple, BoardSquareNames.BalticAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.LightBlue" /> color
        ///     property Oriental Ave.
        /// </summary>
        public static ColorProperty OrientalAve =
            new ColorProperty(100, 6, 30, 90, 270, 400, 550, ColorGroup.LightBlue, BoardSquareNames.OrientalAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.LightBlue" /> color
        ///     property Vermont Ave.
        /// </summary>
        public static ColorProperty VermontAve =
            new ColorProperty(100, 6, 30, 90, 270, 400, 550, ColorGroup.LightBlue, BoardSquareNames.VermontAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.LightBlue" />  color
        ///     property Connecticut Ave.
        /// </summary>
        public static ColorProperty ConnecticutAve =
            new ColorProperty(120, 8, 40, 100, 300, 450, 600, ColorGroup.LightBlue, BoardSquareNames.ConnecticutAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Pink" />  color property
        ///     St. Charles Place.
        /// </summary>
        public static ColorProperty StCharlesPlace =
            new ColorProperty(140, 10, 50, 150, 450, 625, 750, ColorGroup.Pink, BoardSquareNames.StCharlesPlace);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Pink" /> color property
        ///     States Ave.
        /// </summary>
        public static ColorProperty StatesAve =
            new ColorProperty(140, 10, 50, 150, 450, 625, 750, ColorGroup.Pink, BoardSquareNames.StatesAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Pink" /> color property
        ///     Virginia Ave.
        /// </summary>
        public static ColorProperty VirginiaAve =
            new ColorProperty(160, 12, 60, 180, 500, 700, 900, ColorGroup.Pink, BoardSquareNames.VirginiaAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Orange" /> color
        ///     property St. James Place.
        /// </summary>
        public static ColorProperty StJamesPlace =
            new ColorProperty(180, 14, 70, 200, 550, 750, 950, ColorGroup.Orange, BoardSquareNames.StJamesPlace);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Orange" /> color
        ///     property Tennessee Ave.
        /// </summary>
        public static ColorProperty TennesseeAve =
            new ColorProperty(180, 14, 70, 200, 550, 750, 950, ColorGroup.Orange, BoardSquareNames.TennesseeAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Orange" /> color
        ///     property New York Ave.
        /// </summary>
        public static ColorProperty NewYorkAve =
            new ColorProperty(200, 16, 80, 220, 600, 800, 1000, ColorGroup.Orange, BoardSquareNames.NewYorkAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Red" /> color property
        ///     Kentucky Ave.
        /// </summary>
        public static ColorProperty KentuckyAve =
            new ColorProperty(220, 18, 90, 250, 700, 875, 1050, ColorGroup.Red, BoardSquareNames.KentuckyAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Red" /> color property
        ///     Indiana Ave.
        /// </summary>
        public static ColorProperty IndianaAve =
            new ColorProperty(220, 18, 90, 250, 700, 875, 1050, ColorGroup.Red, BoardSquareNames.IndianaAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Red" /> color property
        ///     Illinois Ave.
        /// </summary>
        public static ColorProperty IllinoisAve =
            new ColorProperty(240, 20, 100, 300, 750, 925, 1100, ColorGroup.Red, BoardSquareNames.IllinoisAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Yellow" /> color
        ///     property Atlantic Ave.
        /// </summary>
        public static ColorProperty AtlanticAve =
            new ColorProperty(260, 22, 110, 330, 800, 975, 1150, ColorGroup.Yellow, BoardSquareNames.AtlanticAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Yellow" /> color
        ///     property Ventnor Ave.
        /// </summary>
        public static ColorProperty VentnorAve =
            new ColorProperty(260, 22, 110, 330, 800, 975, 1150, ColorGroup.Yellow, BoardSquareNames.VentnorAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Yellow" /> color
        ///     property Marvin Gardens.
        /// </summary>
        public static ColorProperty MarvinGardens =
            new ColorProperty(280, 24, 120, 360, 850, 1025, 1200, ColorGroup.Yellow, BoardSquareNames.MarvinGardens);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Green" /> color property
        ///     Pacific Ave.
        /// </summary>
        public static ColorProperty PacificAve =
            new ColorProperty(300, 26, 130, 390, 900, 1100, 1275, ColorGroup.Green, BoardSquareNames.PacificAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Green" /> color property
        ///     North Carolina Ave.
        /// </summary>
        public static ColorProperty NorthCarolinaAve =
            new ColorProperty(300, 26, 130, 390, 900, 1100, 1275, ColorGroup.Green, BoardSquareNames.NorthCarolinaAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.Green" /> color property
        ///     Pennsylvania Ave.
        /// </summary>
        public static ColorProperty PennsylvaniaAve =
            new ColorProperty(320, 28, 150, 450, 1000, 1200, 1400, ColorGroup.Green, BoardSquareNames.PennsylvaniaAve);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.DarkBlue" /> color
        ///     property Park Place.
        /// </summary>
        public static ColorProperty ParkPlace =
            new ColorProperty(350, 35, 175, 500, 1100, 1300, 1500, ColorGroup.DarkBlue, BoardSquareNames.ParkPlace);

        /// <summary>
        ///     The default <see cref="Monopoly.ColorGroup.DarkBlue" /> color
        ///     property Boardwalk.
        /// </summary>
        public static ColorProperty Boardwalk =
            new ColorProperty(400, 50, 200, 600, 1400, 1700, 2000, ColorGroup.DarkBlue, BoardSquareNames.Boardwalk);

        /// <summary>
        ///     Constructs a new color property with all the specified information.
        /// </summary>
        /// <param name="price">
        ///     <see cref="Price" />
        /// </param>
        /// <param name="rent">
        ///     <see cref="Rent" />
        /// </param>
        /// <param name="house1Rent">
        ///     <see cref="House1Rent" />
        /// </param>
        /// <param name="house2Rent">
        ///     <see cref="House2Rent" />
        /// </param>
        /// <param name="house3Rent">
        ///     <see cref="House3Rent" />
        /// </param>
        /// <param name="house4Rent">
        ///     <see cref="House4Rent" />
        /// </param>
        /// <param name="hotelRent">
        ///     <see cref="HotelRent" />
        /// </param>
        /// <param name="colorGroup">
        ///     <see cref="ColorGroup" />
        /// </param>
        /// <param name="name">
        ///     <see cref="Name" />
        /// </param>
        public ColorProperty(int price, int rent, int house1Rent, int house2Rent, int house3Rent, int house4Rent,
            int hotelRent, ColorGroup colorGroup, [Localizable(true)] string name)
        {
            Price = price;
            Rent = rent;
            House1Rent = house1Rent;
            House2Rent = house2Rent;
            House3Rent = house3Rent;
            House4Rent = house4Rent;
            HotelRent = hotelRent;
            ColorGroup = colorGroup;
            Name = name;
        }

        /// <inheritdoc />
        public override int Price { get; }

        /// <summary>
        ///     The rent for this property when unimproved.
        /// </summary>
        public int Rent { get; }

        /// <summary>
        ///     The rent for this property when one house has been built.
        /// </summary>
        public int House1Rent { get; }

        /// <summary>
        ///     The rent for this property when two houses have been built.
        /// </summary>
        public int House2Rent { get; }

        /// <summary>
        ///     The rent for this property when three houses have been built.
        /// </summary>
        public int House3Rent { get; }

        /// <summary>
        ///     The rent for this property when four houses have been built.
        /// </summary>
        public int House4Rent { get; }

        /// <summary>
        ///     The rent for this property when a hotel has been built.
        /// </summary>
        public int HotelRent { get; }

        /// <summary>
        ///     The color group of this property.
        /// </summary>
        public ColorGroup ColorGroup { get; }

        /// <inheritdoc />
        public override string Name { get; }

        public virtual int MaxHouses => 5;

        /// <summary>
        ///     A list of all default color properties in order.
        /// </summary>
        public new static List<ColorProperty> AllDefault() => new List<ColorProperty>
        {
            MediterraneanAve,
            BalticAve,
            OrientalAve,
            VermontAve,
            ConnecticutAve,
            StCharlesPlace,
            StatesAve,
            VirginiaAve,
            StJamesPlace,
            TennesseeAve,
            NewYorkAve,
            KentuckyAve,
            IndianaAve,
            IllinoisAve,
            AtlanticAve,
            VentnorAve,
            MarvinGardens,
            PacificAve,
            NorthCarolinaAve,
            PennsylvaniaAve,
            ParkPlace,
            Boardwalk
        };

        /// <inheritdoc />
        public override int GetRent(Game game)
        {
            var state = game[this] ?? throw new UnownedPropertyException();
            if (!state.Owner.Properties.ContainsAll(game.Properties.Where(v =>
                v is ColorProperty c && c.ColorGroup == ColorGroup)))
            {
                return Rent;
            }
            else
            {
                switch (state.Houses)
                {
                    case 0: return Rent * 2;
                    case 1: return House1Rent;
                    case 2: return House2Rent;
                    case 3: return House3Rent;
                    case 4: return House4Rent;
                    case 5: return HotelRent;
                    default: throw new InvalidPropertyStateException($"{nameof(state.Houses)} = {state.Houses}");
                }
            }
        }

        /// <inheritdoc />
        public override string GetInfo(Game game)
        {
            var state = game[this];
            var builder = new StringBuilder();
            if (state == null)
            {
                builder.AppendLine(string.Format(PropertyInfo.Price, Price));
            }
            else
            {
                builder.AppendLine(string.Format(PropertyInfo.Owner, state.Owner));
                builder.AppendLine(string.Format(PropertyInfo.Mortgaged, state.IsMortgaged));
                builder.AppendLine(state.Houses == 5
                    ? PropertyInfo.ColorHotelStatus
                    : string.Format(PropertyInfo.ColorHouseCount, state.Houses));
            }
            builder.AppendLine(string.Format(PropertyInfo.ColorBaseRent, Rent));
            builder.AppendLine(string.Format(PropertyInfo.Color1House, House1Rent));
            builder.AppendLine(string.Format(PropertyInfo.Color2Houses, House2Rent));
            builder.AppendLine(string.Format(PropertyInfo.Color3Houses, House3Rent));
            builder.AppendLine(string.Format(PropertyInfo.Color4Houses, House4Rent));
            builder.AppendLine(string.Format(PropertyInfo.ColorHotel, HotelRent));
            if (state != null)
            {
                builder.AppendLine(string.Format(PropertyInfo.EffectiveRent, GetRent(state.Game)));
            }
            return builder.ToString();
        }

        public override PropertyState CreateState(Player owner) => new ColorPropertyState(this, owner);

        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("price", Price);
            info.AddValue("rent", Rent);
            info.AddValue("house1rent", House1Rent);
            info.AddValue("house2rent", House2Rent);
            info.AddValue("house3rent", House3Rent);
            info.AddValue("house4rent", House4Rent);
            info.AddValue("hotelrent", HotelRent);
            info.AddValue("colorgroup", ColorGroup);
            info.AddValue("name", Name);
        }
    }
}
