﻿using System.Runtime.CompilerServices;

namespace Monopoly
{
    /// <summary>
    ///     Contains all the components of a game of Monopoly.
    /// </summary>
    /// <remarks>
    ///     Any method that triggers user input can throw a
    ///     <see cref="GameSaveInterrupt" />. Do not catch this unless you are
    ///     certain that <see cref="Game.Run" /> is not above you in the call
    ///     stack.
    /// </remarks>
    [CompilerGenerated]
    public class NamespaceDoc { }
}
