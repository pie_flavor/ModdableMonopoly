﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     The Go square.
    /// </summary>
    /// <remarks>
    ///     Despite what you may assume, <see cref="HandleLand" /> does nothing.
    ///     Collecting $200 from passing is instead handled by the
    ///     <see cref="Game" />.
    /// </remarks>
    [Serializable]
    public class Go : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.Go;

        /// <inheritdoc />
        public override void HandleLand(Player player) { }

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }
    }
}
