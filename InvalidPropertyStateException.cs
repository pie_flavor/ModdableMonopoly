﻿using System;

namespace Monopoly
{
    /// <summary>
    ///     Thrown if a property is in an invalid state, such as having a
    ///     <see cref="ColorPropertyState.Houses" /> value of more than 5.
    /// </summary>
    public class InvalidPropertyStateException : Exception
    {
        /// <summary>
        ///     Constructs a new <see cref="InvalidPropertyStateException" /> with
        ///     the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public InvalidPropertyStateException(string message) : base(message) { }
    }
}
