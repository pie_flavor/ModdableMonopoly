﻿using System;
using System.Runtime.Serialization;

namespace Monopoly
{
    [Serializable]
    public class FreeParking : BoardSquare
    {
        /// <inheritdoc />
        public override string Name => BoardSquareNames.FreeParking;

        protected override void GetObjectData(SerializationInfo info, StreamingContext context) { }

        /// <inheritdoc />
        public override void HandleLand(Player player) { }
    }
}
