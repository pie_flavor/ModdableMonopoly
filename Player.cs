﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     A player in the game.
    /// </summary>
    [Serializable]
    public class Player : ISerializable
    {
        public delegate void BankruptedHandler(Player player, BankruptedEventArgs args);

        public delegate void JailedHandler(Player player, JailedEventArgs args);

        public delegate void PropertyPurchasedHandler(Player player, PropertyPurchasedEventArgs args);

        public delegate void PropertyTransferredHandler(Player player, PropertyTransferredEventArgs args);

        private readonly Dictionary<IMod, IData> _data = new Dictionary<IMod, IData>();

        private readonly Queue<string> _messages = new Queue<string>();

        private readonly HashSet<Property> _properties = new HashSet<Property>();

        private bool _inJail = false;

        /// <summary>
        ///     Constructs a new player with the specified name.
        /// </summary>
        /// <param name="game">The game this player is in.</param>
        /// <param name="name">The name of the player.</param>
        public Player(Game game, string name)
        {
            Name = name;
            Game = game;
        }

        /// <summary>
        ///     The game that this player is a part of.
        /// </summary>
        public Game Game { get; }

        /// <summary>
        ///     A queue of all messages that this player has yet to receive.
        /// </summary>
        public IReadOnlyCollection<string> Messages => _messages;

        /// <summary>
        ///     The amount of money this player has.
        /// </summary>
        /// <remarks>
        ///     Do not checked subtract; there are other ways a player can pay.
        /// </remarks>
        /// <seealso cref="Pay(int)" />
        public int Money { get; set; } = 1_500;

        /// <summary>
        ///     A collection of all the properties this player owns.
        /// </summary>
        public IReadOnlyCollection<Property> Properties => _properties;

        /// <summary>
        ///     How many Get Out Of Jail Free cards this player owns.
        /// </summary>
        public int GetOutOfJailFreeCards { get; set; } = 0;

        /// <summary>
        ///     The name of this player.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Whether the player is in jail. When set to true, moves the player
        ///     to <see cref="Jail" />.
        /// </summary>
        public bool InJail
        {
            get => _inJail;
            set
            {
                if (value && !_inJail)
                {
                    JailTurns = 0;
                    Game.SetSquare<Jail>(this, false);
                    Notify(Strings.YouAreJailed);
                    Jailed?.Invoke(this, JailedEventArgs.Empty);
                }
                _inJail = value;
            }
        }

        /// <summary>
        ///     The number of turns this player has spent in jail. When this hits
        ///     three, they are required to pay bail.
        /// </summary>
        public int JailTurns { get; set; } = 0;

        /// <summary>
        ///     Whether or not this player has been bankrupted.
        /// </summary>
        public bool IsBankrupted { get; private set; } = false;

        public IData this[IMod mod]
        {
            get => _data.TryGetValue(mod, out var data) ? data : null;
            set => _data[mod] = value;
        }

        public IData this[string modId]
        {
            get => Game.Mods.TryGetValue(modId, out var mod) ? this[mod] : null;
            set
            {
                if (Game.Mods.TryGetValue(modId, out var mod))
                {
                    this[mod] = value;
                }
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            var data = new Dictionary<string, IData>();
            foreach (var pair in _data)
            {
                data[pair.Key.Id] = pair.Value;
            }
            info.AddValue("data", data);
            info.AddValue("messages", _messages);
            info.AddValue("injail", _inJail);
            info.AddValue("money", Money);
            info.AddValue("name", Name);
            info.AddValue("jailturns", JailTurns);
            info.AddValue("getoutofjailfreecards", GetOutOfJailFreeCards);
            info.AddValue("isbankrupted", IsBankrupted);
        }

        public event JailedHandler Jailed;

        /// <summary>
        ///     The total amount this player can possibly pay. Check this before
        ///     calling <see cref="Pay(int)" /> if the purchase is optional.
        /// </summary>
        /// <returns></returns>
        public int GetTotalPayable()
        {
            int value = Money;
            foreach (var property in _properties)
            {
                var state = Game[property];
                if (state.IsMortgaged)
                {
                    continue;
                }
                value += property.MortgageValue;
                if (!(property is ColorProperty cproperty))
                {
                    continue;
                }
                var cstate = (ColorPropertyState)state;
                value += cstate.Houses * cproperty.ColorGroup.HousePrice;
            }
            return value;
        }

        /// <summary>
        ///     Messages the player. Text wrapped in <c>[]</c> will be colored.
        /// </summary>
        /// <remarks>
        ///     Please use this instead of <see cref="IShell.Write" /> directly, as
        ///     there is no guarantee that the message will get displayed to the
        ///     correct player otherwise.
        /// </remarks>
        /// <param name="message">The message to send.</param>
        public void Notify([Localizable(true)] string message)
        {
            if (Game.CurrentInput == this)
            {
                Console.WriteLine(message);
            }
            else
            {
                _messages.Enqueue(message);
            }
        }

        public void FlushMessages()
        {
            int size = _messages.Count;
            foreach (string message in _messages.Drain())
            {
                Notify(message);
                size--;
                if (size == 0)
                {
                    break;
                }
            }
        }

        public event PropertyPurchasedHandler PropertyPurchased;

        /// <summary>
        ///     Requests that the player purchase a particular unowned property.
        /// </summary>
        /// <param name="property">The property to purchase.</param>
        /// <param name="auction">
        ///     Whether or not refusing the purchase should result in auctioning
        ///     the property.
        /// </param>
        /// <returns>Whether or not they purchased the property.</returns>
        /// <exception cref="OwnedPropertyException">
        ///     If the property is already owned.
        /// </exception>
        public void RequestPurchase(Property property, bool auction = true)
        {
            if (Game[property] != null)
            {
                throw new OwnedPropertyException();
            }
            Game.CurrentInput = this;
            if (GetTotalPayable() < property.Price)
            {
                Notify(Strings.CannotAffordProperty);
            }
            else if (Game.Shell.Confirm(Strings.PropertyPurchaseQuery))
            {
                var args = new PropertyPurchasedEventArgs(property, false);
                PropertyPurchased?.Invoke(this, args);
                if (!args.Cancelled)
                {
                    Pay(property.Price);
                    Notify(string.Format(Strings.BoughtProperty, property.Name));
                    TransferOwnership(property);
                }
            }
            else if (auction)
            {
                Game.AuctionProperty(property);
            }
            Game.CurrentInput = null;
        }

        /// <summary>
        ///     Pays another player a specified amount of money, or handles
        ///     bankruptcy.
        /// </summary>
        /// <remarks>
        ///     If the payment is optional, make sure to check
        ///     <see cref="GetTotalPayable" /> first.
        /// </remarks>
        /// <param name="to">Who to send the money to.</param>
        /// <param name="amount">The amount of money to send.</param>
        public void Pay(Player to, int amount)
        {
            if (Money >= amount)
            {
                Money -= amount;
                if (to != null)
                {
                    to.Money += amount;
                }
            }
            else
            {
                if (GetTotalPayable() >= amount)
                {
                    Game.CurrentInput = this;
                    Notify(string.Format(Strings.OweMoney, to?.Name ?? Strings.TheBank, amount, Money));
                    Notify(Strings.ObtainMoneyQuery);
                    while (Money < amount)
                    {
                        string cmd = Game.Shell.Expect(Strings.ObtainMoneyChoice, Strings.TurnChoiceMortgage,
                            Strings.ObtainMoneyChoiceSell);
                        if (cmd.Equals("mortgage", StringComparison.CurrentCultureIgnoreCase))
                        {
                            var property = Game.Shell.Expect(Strings.EnterPropertyName,
                                Game.Properties, p => p.Name);
                            var state = Game[property];
                            if (state == null)
                            {
                                Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (state.Owner != this)
                            {
                                Notify(string.Format(Strings.NoOwnPropertyOther, state.Owner));
                                continue;
                            }

                            if (state.IsMortgaged)
                            {
                                Notify(Strings.AlreadyMortgaged);
                                continue;
                            }

                            if (property is ColorProperty cproperty)
                            {
                                var others = Game.Properties.WhereCast<ColorProperty>()
                                    .Where(c => c.ColorGroup == cproperty.ColorGroup)
                                    .Select(p => Game[p]).Where(s => s != null);
                                var notEnough = others.FirstOrDefault(s => s.Houses > 0);
                                if (notEnough != null)
                                {
                                    Notify(string.Format(Strings.PropertyHasHouses, notEnough));
                                    continue;
                                }
                            }

                            state.IsMortgaged = true;
                            Money += property.MortgageValue;
                        }
                        else
                        {
                            var cproperty = Game.Shell.Expect(Strings.EnterPropertyName,
                                Game.Properties.WhereCast<ColorProperty>(), c => c.Name);
                            var cstate = Game[cproperty];
                            if (cstate == null)
                            {
                                Notify(Strings.NoOwnProperty);
                                continue;
                            }
                            else if (cstate.Owner != this)
                            {
                                Notify(string.Format(Strings.NoOwnPropertyOther, cstate.Owner));
                                continue;
                            }

                            if (cstate.Houses == 0)
                            {
                                Notify(Strings.NoHousesOnProperty);
                                continue;
                            }

                            var others = Game.Properties.WhereCast<ColorProperty>()
                                .Where(c => c.ColorGroup == cproperty.ColorGroup).Except(cproperty)
                                .Select(c => Game[c]).Where(s => s != null);
                            var notEnough = others.FirstOrDefault(s => s.Houses > cstate.Houses);
                            if (notEnough != null)
                            {
                                Notify(string.Format(Strings.TooManyHouses, notEnough));
                                continue;
                            }

                            cstate.Houses--;
                            Money += cproperty.ColorGroup.HouseSellPrice;
                        }
                    }

                    if (to != null)
                    {
                        to.Money += amount;
                    }

                    Money -= amount;
                    Game.CurrentInput = null;
                }
                else
                {
                    Bankrupt(to);
                }
            }
        }

        public event BankruptedHandler Bankrupted;

        private void Bankrupt(Player to)
        {
            Bankrupted?.Invoke(this, new BankruptedEventArgs(to));
            Notify(Strings.BankruptDeclaration);
            if (to == null)
            {
                foreach (var property in Properties)
                {
                    Game[property] = null;
                }
            }
            else
            {
                to.Money += Money;
                to.GetOutOfJailFreeCards += GetOutOfJailFreeCards;
                var mortgaged = new List<PropertyState>(Properties.Count);
                foreach (var property in Properties)
                {
                    var state = Game[property];
                    if (state is ColorPropertyState cstate)
                    {
                        to.Money += cstate.Houses * ((ColorProperty)property).ColorGroup.HouseSellPrice;
                    }
                    state.Owner = to;
                    to._properties.Add(property);
                    if (state.IsMortgaged)
                    {
                        mortgaged.Add(state);
                    }
                }
                if (mortgaged.Count > 0)
                {
                    Game.CurrentInput = to;
                    to.Notify(
                        string.Format(Strings.ReceivingMortgagedProperties, this));
                    int totalInterest = mortgaged.Select(p => p.Property.UnmortgageInterest).Sum();
                    if (totalInterest > to.GetTotalPayable())
                    {
                        to.Notify(
                            Strings.BankruptFromBankruptcy);
                        to.Bankrupt(null);
                    }
                    else
                    {
                        to.Notify(Strings.MustPayInterest);
                        foreach (var state in mortgaged)
                        {
                            if (to.GetTotalPayable() < totalInterest - state.Property.UnmortgageInterest +
                                state.Property.UnmortgageValue &&
                                Game.Shell.Confirm(string.Format(Strings.UnmortgageQuery, state,
                                    state.Property.UnmortgageValue)))
                            {
                                to.Pay(state.Property.UnmortgageValue);
                                to.Notify(string.Format(Strings.UnmortgagedProperty, state));
                                state.IsMortgaged = false;
                            }
                            else
                            {
                                to.Pay(state.Property.UnmortgageInterest);
                                to.Notify(string.Format(Strings.PaidInterest, state.Property.UnmortgageInterest,
                                    state));
                            }
                        }
                    }
                    Game.CurrentInput = null;
                }
                IsBankrupted = true;
            }
        }

        public event PropertyTransferredHandler PropertyTransferred;

        public void TransferOwnership(Property property, bool handleMortgage = true)
        {
            var state = Game[property];
            var args = new PropertyTransferredEventArgs(property, state, false, handleMortgage);
            if (state == null)
            {
                args.State = property.CreateState(this);
                PropertyTransferred?.Invoke(this, args);
                if (args.Cancelled)
                {
                    Game[property] = args.State;
                    _properties.Add(property);
                }
            }
            else
            {
                PropertyTransferred?.Invoke(this, args);
                if (!args.Cancelled)
                {
                    args.State.Owner._properties.Remove(property);
                    args.State.Owner = this;
                    _properties.Add(property);
                    if (args.HandleMortgage && args.State.IsMortgaged)
                    {
                        Game.CurrentInput = this;
                        if (Game.Shell.Confirm(Strings.ReceivingMortgagedProperty))
                        {
                            Pay(property.UnmortgageValue);
                            args.State.IsMortgaged = false;
                        }
                        else
                        {
                            Pay(property.UnmortgageInterest);
                        }
                        Game.CurrentInput = null;
                    }
                    Game[property] = args.State;
                }
            }
        }

        /// <summary>
        ///     Pays the Bank a specified amount of money, or handles bankruptcy.
        /// </summary>
        /// <remarks>
        ///     If the payment is optional, make sure to check
        ///     <see cref="GetTotalPayable" /> first.
        /// </remarks>
        /// <param name="money">The amount of money to send.</param>
        public void Pay(int money)
        {
            Pay(null, money);
        }

        /// <inheritdoc />
        public override string ToString() => Name;
    }
}
