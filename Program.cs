﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Monopoly
{
    public static class Program
    {
        public delegate void GameCreatedHandler(object sender, GameCreatedEventArgs args);

        public delegate void GameCreateHandler(object sender, GameCreateEventArgs args);

        public delegate void GameLoadedHandler(object sender, GameLoadedEventArgs args);

        public static event GameCreatedHandler GameCreated;

        public static event GameCreateHandler GameCreate;

        public static event GameLoadedHandler GameLoaded;

        private static void Main()
        {
            Game game;
            var shell = new Shell();
            var dir = Directory.CreateDirectory("mods");
            var dict = new Dictionary<string, IMod>();
            foreach (var file in dir.EnumerateFiles("*.dll"))
            {
                try
                {
                    var assembly = Assembly.LoadFile(file.FullName);
                    foreach (var type in assembly.GetExportedTypes().Where(t => t.IsSubclassOf(typeof(IMod))))
                    {
                        var mod = (IMod)type.GetConstructor(new Type[] { })?.Invoke(new object[] { });
                        if (mod == null)
                        {
                            continue;
                        }
                        dict[mod.Id] = mod;
                        shell.Write(string.Format(Strings.LoadedMod, mod.Name));
                    }
                }
                catch (BadImageFormatException) { }
            }
            if (shell.Confirm(Strings.LoadGame))
            {
                var openDialog = new OpenFileDialog
                {
                    Filter = $@"{Strings.DatFiles} (*.dat)|*.dat",
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory),
                    Multiselect = false
                };
                var res = openDialog.ShowDialog();
                if (res != DialogResult.OK)
                {
                    return;
                }
                using (var stream = openDialog.OpenFile())
                {
                    game = Game.Load(stream);
                    GameLoaded?.Invoke(null, new GameLoadedEventArgs(game));
                }
            }
            else
            {
                string names = shell.Expect(Strings.EnterNames);
                var board = Board.DefaultBoard();
                var chance = Card.DefaultChance();
                var communityChest = Card.DefaultCommunityChest();
                GameCreate?.Invoke(null, new GameCreateEventArgs(shell, board, chance, communityChest));
                game = new Game(shell, new Board(board), names.Split(' '), Card.DefaultChance(),
                    Card.DefaultCommunityChest(), dict);
                GameCreated?.Invoke(null, new GameCreatedEventArgs(game));
                GameLoaded?.Invoke(null, new GameLoadedEventArgs(game));
                game.UpdatePlayers();
            }
            var saveDialog = new SaveFileDialog
            {
                Filter = $@"{Strings.DatFiles} (*.dat)|*.dat",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)
            };
            bool save;
            while ((save = game.Run()) && saveDialog.ShowDialog() != DialogResult.OK) { }
            if (!save)
            {
                return;
            }
            using (var stream = saveDialog.OpenFile())
            {
                game.Save(stream);
            }
        }
    }
}
