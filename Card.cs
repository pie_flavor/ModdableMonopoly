﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace Monopoly
{
    /// <summary>
    ///     A card which is picked up on the <see cref="Chance" /> or
    ///     <see cref="CommunityChest" /> squares.
    /// </summary>
    [Serializable]
    public class Card : ISerializable
    {
        /// <summary>
        ///     The default <c>Advance to Go</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card AdvanceToGo =
            new Card(CardNames.AdvanceToGo, p => p.Game.SetSquare<Go>(p));

        /// <summary>
        ///     The default <c>Bank pays you divident of $50</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card BankDividend =
            new Card(CardNames.BankDividend, p => p.Money += 50);

        /// <summary>
        ///     The default <c>Go back 3 spaces</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card Back3 =
            new Card(CardNames.Back3, p => p.Game.Advance(p, -3, false));

        /// <summary>
        ///     The default <c>Advance to nearest utility</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card NearestUtility = new Card(CardNames.NearestUtility, p =>
        {
            p.Game.SetSquare<Utility>(p, handleLand: false);
            var utility = (Utility)p.Game[p];
            var state = p.Game[utility];
            if (state == null)
            {
                p.Notify(string.Format(Strings.PropertyUnownedIrregular, utility));
                p.RequestPurchase(utility, false);
            }
            else if (state.Owner != p)
            {
                p.Notify(string.Format(Strings.HighestRentOwed, utility, state.Owner));
                int amount = p.Game.LastDieRoll * 10;
                p.Pay(state.Owner, amount);
                p.Notify(string.Format(Strings.RentPaid, amount, state.Owner));
                state.Owner.Notify(string.Format(Strings.RentReceived, p, amount));
            }
        });

        /// <summary>
        ///     The default <c>Go directly to Jail</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card GoToJail = new Card(CardNames.GoToJail, p => p.InJail = true);

        /// <summary>
        ///     The default <c>Pay poor tax of $15</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card PoorTax = new Card(CardNames.PoorTax, p => p.Pay(15));

        /// <summary>
        ///     The default <c>Advance to St. Charles Place</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card StCharles = new Card(CardNames.StCharles,
            p => p.Game.SetSquare(p, ColorProperty.StCharlesPlace));

        /// <summary>
        ///     The default <c>You have been elected Chairman of the Board</c>
        ///     <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card ChairmanOfTheBoard = new Card(
            CardNames.ChairmanOfTheBoard,
            p =>
            {
                foreach (var p2 in p.Game.Players.Except(p))
                {
                    p.Pay(p2, 50);
                    p2.Notify(string.Format(Strings.ElectedChairmanOfBoard, p));
                }
            });

        /// <summary>
        ///     The default <c>Advance to nearest railroad</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card NearestRailroad = new Card(CardNames.NearestRailroad, p =>
        {
            p.Game.SetSquare<Railroad>(p, handleLand: false);
            var railroad = (Railroad)p.Game[p];
            var state = p.Game[railroad];
            if (state == null)
            {
                p.Notify(string.Format(Strings.PropertyUnownedIrregular, railroad));
                p.RequestPurchase(railroad, false);
            }
            else if (state.Owner != p)
            {
                p.Notify(string.Format(Strings.DoubleRentOwed, railroad, state.Owner));
                int amount = railroad.GetRent(p.Game) * 2;
                p.Pay(state.Owner, amount);
                p.Notify(string.Format(Strings.RentPaid, amount, state.Owner));
                state.Owner.Notify(string.Format(Strings.RentReceived, p, amount));
            }
        });

        /// <summary>
        ///     The default <c>Take a ride on the Reading</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card ReadingRailroad = new Card(CardNames.ReadingRailroad,
            p => p.Game.SetSquare(p, Railroad.ReadingRailroad));

        /// <summary>
        ///     The default <c>Take a walk on the Boardwalk</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card Boardwalk = new Card(CardNames.Boardwalk,
            p => p.Game.SetSquare(p, ColorProperty.Boardwalk));

        /// <summary>
        ///     The default <c>Your building and loan matures</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card BuildingAndLoan =
            new Card(CardNames.BuildingAndLoan, p => p.Money += 50);

        /// <summary>
        ///     The default <c>Advance to Illinois Ave</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card IllinoisAve = new Card(CardNames.IllinoisAve,
            p => p.Game.SetSquare(p, ColorProperty.IllinoisAve));

        /// <summary>
        ///     The default <c>Get out of jail free</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card GetOutOfJailFree =
            new Card(CardNames.GetOutOfJailFree, p => p.GetOutOfJailFreeCards++);

        /// <summary>
        ///     The default <c>Make general repairs on all your property</c> <see cref="Chance" /> card.
        /// </summary>
        public static readonly Card GeneralRepairs =
            new Card(
                CardNames.GeneralRepairs,
                p =>
                {
                    int total = p.Properties.WhereCast<ColorProperty>().Select(c => p.Game[c].Houses).Sum(h =>
                        h == 5 ? 100 :
                        h > 0 ? 25 : 0);
                    p.Pay(total);
                    p.Notify(string.Format(Strings.PaidTotal, total));
                });

        public static readonly Card XmasFund = new Card(CardNames.XmasFund, p => p.Money += 100);

        public static readonly Card Inherit100 = new Card(CardNames.Inherit100, p => p.Money += 100);

        public static readonly Card StockSale = new Card(CardNames.StockSale, p => p.Money += 45);

        public static readonly Card BankError = new Card(CardNames.BankError, p => p.Money += 200);

        public static readonly Card PayHospital = new Card(CardNames.PayHospital, p => p.Pay(100));

        public static readonly Card DoctorsFee = new Card(CardNames.DoctorsFee, p => p.Pay(50));

        public static readonly Card ReceiveForServices = new Card(CardNames.ReceiveForServices, p => p.Money += 25);

        public static readonly Card SchoolTax = new Card(CardNames.SchoolTax, p => p.Pay(150));

        public static readonly Card BeautyContest =
            new Card(CardNames.BeautyContest, p => p.Money += 10);

        public static readonly Card GrandOpera =
            new Card(CardNames.GrandOpera, p =>
            {
                foreach (var player in p.Game.Players.Except(p))
                {
                    player.Pay(p, 50);
                    player.Notify(string.Format(Strings.PaidTo, 50, p));
                }
            });

        public static readonly Card IncomeTaxRefund = new Card(CardNames.IncomeTaxRefund, p => p.Money += 20);

        public static readonly Card StreetRepairs =
            new Card(CardNames.StreetRepairs, p =>
            {
                int total = p.Properties.WhereCast<ColorProperty>().Select(c => p.Game[c].Houses).Sum(h =>
                    h == 5 ? 115 :
                    h > 0 ? 40 : 0);
                p.Pay(total);
                p.Notify(string.Format(Strings.PaidTotal, total));
            });

        public static readonly Card LifeInsurance =
            new Card(CardNames.LifeInsurance, p => p.Money += 100);

        /// <summary>
        ///     Constructs a new <see cref="Card" /> with the specified
        ///     message and action.
        /// </summary>
        /// <param name="message">The message of the card.</param>
        /// <param name="action">The action of this card.</param>
        private Card([Localizable(true)] string message, CardAction action)
        {
            Action = action;
            Message = message;
            ModId = "Monopoly";
        }

        public Card([Localizable(true)] string message, string modId, CardAction action)
        {
            Action = action;
            Message = message;
            ModId = modId;
        }

        /// <summary>
        ///     The <see cref="CardAction" /> that is done when this card is selected.
        /// </summary>
        public CardAction Action { get; }

        /// <summary>
        ///     The message of this card.
        /// </summary>
        public string Message { get; }

        public string ModId { get; }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("modid", ModId);
            info.AddValue("message", Message);
        }

        public static List<Card> DefaultChance() => new List<Card>
        {
            AdvanceToGo,
            IllinoisAve,
            StCharles,
            NearestUtility,
            NearestRailroad,
            NearestRailroad,
            BankDividend,
            GetOutOfJailFree,
            Back3,
            GoToJail,
            GeneralRepairs,
            PoorTax,
            ReadingRailroad,
            Boardwalk,
            ChairmanOfTheBoard,
            BuildingAndLoan
        };

        public static List<Card> DefaultCommunityChest() => new List<Card>
        {
            AdvanceToGo,
            BankError,
            DoctorsFee,
            StockSale,
            GetOutOfJailFree,
            GoToJail,
            GrandOpera,
            XmasFund,
            IncomeTaxRefund,
            LifeInsurance,
            PayHospital,
            SchoolTax,
            ReceiveForServices,
            StreetRepairs,
            BeautyContest,
            Inherit100
        };
    }

    public delegate void CardAction(Player player);
}
