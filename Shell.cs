﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Monopoly
{
    /// <summary>
    ///     An implementation of <see cref="IShell" /> which writes to stdout and
    ///     reads from stdin.
    /// </summary>
    public class Shell : IShell
    {
        /// <inheritdoc />
        public Game Game { get; set; }

        /// <inheritdoc />
        public bool CanSave { get; set; }

        /// <inheritdoc />
        public string Expect([Localizable(true)] string prompt = null)
        {
            if (prompt != null)
            {
                Write(prompt);
            }
            return Read();
        }

        /// <inheritdoc />
        public string Expect([Localizable(true)] string prompt, Regex expected, bool silent = false)
        {
            if (!silent)
            {
                Write(prompt);
            }
            while (true)
            {
                string str = Read();
                if (expected.IsMatch(str))
                {
                    return str;
                }
                Write(Strings.InvalidInput);
                Write(prompt);
            }
        }

        /// <inheritdoc />
        public string Expect([Localizable(true)] string prompt, [Localizable(true)] params string[] expected)
        {
            Write(prompt);
            while (true)
            {
                string str = Read();
                foreach (string item in expected)
                {
                    if (item.Equals(str, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return item;
                    }
                }
                Write(Strings.InvalidInput);
                Write(prompt);
            }
        }

        /// <inheritdoc />
        public TResult Expect<TResult>([Localizable(true)] string prompt, OutFunc<string, TResult> transform,
            bool silent = false)
        {
            if (!silent)
            {
                Write(prompt);
            }
            while (true)
            {
                string str = Read();
                if (transform(str, out var result))
                {
                    return result;
                }
                Write(Strings.InvalidInput);
                Write(prompt);
            }
        }

        /// <inheritdoc />
        public string Expect([Localizable(true)] string prompt, IEnumerable<string> expected) =>
            Expect(prompt, expected.ToArray());

        /// <inheritdoc />
        public TResult Expect<TResult>([Localizable(true)] string prompt, IEnumerable<TResult> expected,
            Func<TResult, string> mapper,
            bool silent = false)
        {
            if (!silent)
            {
                Write(prompt);
            }
            var ts = expected.ToArray();
            var dict = new Dictionary<string, TResult>();
            string str = Read().ToLower();
            foreach (var t in ts)
            {
                string repr = mapper(t).ToLower();
                if (repr == str)
                {
                    return t;
                }
                dict[repr] = t;
            }
            do
            {
                Write(prompt);
                if (dict.TryGetValue(Read().ToLower(), out var t))
                {
                    return t;
                }
            } while (true);
        }

        /// <inheritdoc />
        public bool Confirm([Localizable(true)] string prompt) =>
            Expect($@"{prompt} ({Strings.Y}/{Strings.N})", Strings.Y, Strings.N) == "y";

        /// <inheritdoc />
        [Localizable(true)]
        public void Write(string message)
        {
            message = message.Replace(@"|", Environment.NewLine);
            int indexLeft = 0;
            while (indexLeft < message.Length - 1 && (indexLeft = message.IndexOf('[')) != -1)
            {
                int indexRight = message.IndexOf(']', indexLeft);
                if (indexRight == -1)
                {
                    continue;
                }
                var color = Console.ForegroundColor;
                Console.Write(message.Substring(0, indexLeft));
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message.Substring(indexLeft, indexRight - indexLeft + 1));
                Console.ForegroundColor = color;
                message = message.Substring(indexRight + 1);
            }
            Console.WriteLine(message);
        }

        /// <summary>
        ///     Reads from stdin, processing commands.
        /// </summary>
        /// <returns>User input</returns>
        private string Read()
        {
            var player = Game?.CurrentInput;
            while (true)
            {
                string str = Console.ReadLine() ?? throw new EndOfStreamException();
                if (player != null && str[0] == ':')
                {
                    switch (str)
                    {
                        case ":help":
                            Write(@"::player :money :properties :info :jail-cards");
                            continue;
                        case "::player":
                            player = Expect(Strings.EnterPlayerName, Game.Players, p => p.Name);
                            continue;
                        case ":money":
                            Write($@"${player.Money}");
                            continue;
                        case ":properties":
                            Write(string.Join("|", player.Properties.Select(p => p.Name)));
                            continue;
                        case ":info":
                            var prop = Expect(Strings.EnterPropertyName, Game.Properties, p => p.Name);
                            Write(prop.GetInfo(Game));
                            continue;
                        case ":jail-cards":
                            Write($@"{player.GetOutOfJailFreeCards}");
                            continue;
                        case ":location":
                            Write($@"{Game[player]}");
                            continue;
                    }
                }
                return str;
            }
        }
    }
}
